const webpack = require("webpack");
const merge = require("webpack-merge");

const commonConfig = require("./webpack.common");

module.exports = merge(commonConfig, {
  devtool: "eval-source-map",

  mode: "development",

  entry: {
    app: ["webpack-hot-middleware/client?reload=true"]
  },

  output: {
    filename: "js/[name].js",
    chunkFilename: "[id].chunk.js"
  },

  devServer: {
    contentBase: "./client/public",
    historyApiFallback: true,
    stats: "minimal" // none (or false), errors-only, minimal, normal (or true) and verbose
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      },
      {
        test: /\.(png|gif|jpg|cur)$/i,
        loader: "url-loader",
        options: { limit: 8192 }
      },
      {
        test: /\.woff2(\?v=[0-9]\.[0-9]\.[0-9])?$/i,
        loader: "url-loader",
        options: { limit: 10000, mimetype: "application/font-woff2" }
      },
      {
        test: /\.woff(\?v=[0-9]\.[0-9]\.[0-9])?$/i,
        loader: "url-loader",
        options: { limit: 10000, mimetype: "application/font-woff" }
      },
      {
        test: /\.(ttf|eot|svg|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/i,
        loader: "file-loader"
      }
    ]
  }
});
