import React, { Component } from "react";
var util = require("util");
export default class Suggest extends Component {
  state = {
    products: []
  };
  render() {
    return (
      <Card>
        <Image src="/assets/img/suggest1.jpg" />
        <Card.Content>
          <Card.Header>FS Triumph Daytona 500cc 1969</Card.Header>

          <Label attached="top right" size="medium" color="teal" tag>
            Used
          </Label>
          <Card.Description>
            <Label size="huge">Rp.110.000.000</Label>
            <div>
              <Icon floated="right" name="map marker alternate" />
              Bandung
            </div>
          </Card.Description>
        </Card.Content>
        <Card.Content extra>
          <div>
            <a>
              <Icon name="user" />
              10 View
            </a>

            <Button
              floated="right"
              basic
              color="blue"
              onClick={() => this.setState({ navigate: true })}
            >
              More Detail
            </Button>
          </div>
        </Card.Content>
      </Card>
    );
  }
}
