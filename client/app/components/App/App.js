import React, { Component } from "react";
import Header from "../Header/Header-4";
import Footer from "../Footer/Footer";

const App = ({ children }) => (
  <>
    <Header />
    <main style={{ marginTop: "104px" }}>{children}</main>

    <Footer />
  </>
);

export default App;
