import PropTypes from "prop-types";
import React, { Component } from "react";
import {
  Button,
  Container,
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  List,
  Menu,
  Responsive,
  Segment,
  Sidebar,
  Visibility,
  GridRow,
  GridColumn,
  Input,
  Form,
  Dropdown
} from "semantic-ui-react";
import { Link } from "react-router-dom";

let popupShow = "";
/* Heads up!
 * Neither Semantic UI nor Semantic UI React offer a responsive navbar, however, it can be implemented easily.
 * It can be more complicated, but you can create really flexible markup.
 */
class DesktopContainer extends Component {
  state = {};

  hideFixedMenu = () => this.setState({ fixed: false });
  showFixedMenu = () => this.setState({ fixed: true });

  render() {
    const { children } = this.props;
    const { fixed } = this.state;
    /*
<Segment
            inverted
            textAlign="center"
            style={{ padding: "1em 0em" }}
            clearing
          >
*/
    return (
      <Responsive minWidth={Responsive.onlyTablet.minWidth}>
        <Visibility
          once={false}
          onBottomPassed={this.showFixedMenu}
          onBottomPassedReverse={this.hideFixedMenu}
        >
          <Segment inverted textAlign="center" style={{ padding: "1em 0em" }}>
            <Menu fixed={fixed ? "top" : null} inverted right size="large">
              <Menu.Item as="a" position="left">
                <Link to="/">
                  <Image size="small" src="/assets/img/motostation_logo.png" />
                </Link>
              </Menu.Item>
              <Menu.Item>
                <Container>
                  <Form>
                    <Input
                      style={{}}
                      icon={<Icon name="search" inverted circular link />}
                      placeholder="Search..."
                    />
                  </Form>
                </Container>
              </Menu.Item>
              <Menu.Menu position="right">
                <Menu.Item as="a">
                  <Link to="/test/dynamicselect">
                    <Icon name="grid layout" />
                    <strong>Sell Items</strong>
                  </Link>
                </Menu.Item>

                <Menu.Item as="a" link>
                  <Icon name="comment alternate outline" />
                  <strong>Notification</strong>
                </Menu.Item>
                {popupShow}
                <Menu.Item as="a" link>
                  <Icon name="user circle outline" />
                  Cart
                </Menu.Item>
              </Menu.Menu>
            </Menu>

            <Menu
              fixed={fixed ? "top" : null}
              inverted
              pointing={!fixed}
              secondary={!fixed}
              size="large"
              center
            >
              <Grid>
                <GridRow>
                  <DropdownAmerican />
                  <DropdownEuropean />
                  <DropdownJapan />
                </GridRow>
              </Grid>
            </Menu>
          </Segment>
        </Visibility>

        {children}
      </Responsive>
    );
  }
}
//</Segment>
DesktopContainer.propTypes = {
  children: PropTypes.node
};

class MobileContainer extends Component {
  state = {};

  handleSidebarHide = () => this.setState({ sidebarOpened: false });

  handleToggle = () => this.setState({ sidebarOpened: true });

  render() {
    const { children } = this.props;
    const { sidebarOpened } = this.state;

    return (
      <Responsive
        as={Sidebar.Pushable}
        maxWidth={Responsive.onlyMobile.maxWidth}
      >
        <Sidebar
          as={Menu}
          animation="push"
          inverted
          onHide={this.handleSidebarHide}
          vertical
          visible={sidebarOpened}
        >
          <Menu.Item as="a" active>
            Home
          </Menu.Item>
          <Menu.Item as="a">Work</Menu.Item>
          <Menu.Item as="a">Company</Menu.Item>
          <Menu.Item as="a">Careers</Menu.Item>
          <Menu.Item as="a">Log in</Menu.Item>
          <Menu.Item as="a">Sign Up</Menu.Item>
        </Sidebar>

        <Sidebar.Pusher dimmed={sidebarOpened}>
          <Segment
            inverted
            textAlign="center"
            style={{ minHeight: 350, padding: "1em 0em" }}
            vertical
          >
            <Container>
              <Menu inverted pointing secondary size="large">
                <Menu.Item onClick={this.handleToggle}>
                  <Icon name="sidebar" />
                </Menu.Item>
                <Menu.Item position="right">
                  <Button as="a" inverted>
                    Log in
                  </Button>
                  <Button as="a" inverted style={{ marginLeft: "0.5em" }}>
                    Sign Up
                  </Button>
                </Menu.Item>
              </Menu>
            </Container>
          </Segment>

          {children}
        </Sidebar.Pusher>
      </Responsive>
    );
  }
}

MobileContainer.propTypes = {
  children: PropTypes.node
};

const ResponsiveContainer = ({ children }) => (
  <div>
    <DesktopContainer>{children}</DesktopContainer>
    <MobileContainer>{children}</MobileContainer>
  </div>
);

ResponsiveContainer.propTypes = {
  children: PropTypes.node
};
const DropdownAmerican = () => (
  <Dropdown item simple text="American Motorcycle">
    <Dropdown.Menu>
      <Dropdown.Item>
        <a className="text" style={{ color: "black" }} href="/users" />
        <span className="text">Motorcycle</span>
      </Dropdown.Item>
      <Dropdown.Item>
        <a className="text" style={{ color: "black" }} href="/users">
          Spareparts
        </a>
      </Dropdown.Item>
      <Dropdown.Item>
        <a className="text" style={{ color: "black" }} href="/users" />
        <span className="text">Accesoris</span>
      </Dropdown.Item>
    </Dropdown.Menu>
  </Dropdown>
);
const DropdownEuropean = () => (
  <Dropdown item simple text="European Motorcycle">
    <Dropdown.Menu>
      <Dropdown.Item>
        <Icon name="dropdown" />
        <span className="text">Modern European</span>
        <Dropdown.Menu>
          <Dropdown.Item>Motorcycle</Dropdown.Item>
          <Dropdown.Item>Spareparts</Dropdown.Item>
          <Dropdown.Item>Accesoris</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown.Item>
      <Dropdown.Item>
        <Icon name="dropdown" />
        <span className="text">Classic European</span>
        <Dropdown.Menu>
          <Dropdown.Item>Motorcycle</Dropdown.Item>
          <Dropdown.Item>Spareparts</Dropdown.Item>
          <Dropdown.Item>Accesoris</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown.Item>
      <Dropdown.Item>
        <Icon name="dropdown" />
        <span className="text">European Scooter</span>
        <Dropdown.Menu>
          <Dropdown.Item>Scooter</Dropdown.Item>
          <Dropdown.Item>Spareparts</Dropdown.Item>
          <Dropdown.Item>Acessories</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown.Item>
    </Dropdown.Menu>
  </Dropdown>
);
const DropdownJapan = () => (
  <Dropdown item simple text="Japanese Motorcycle">
    <Dropdown.Menu>
      <Dropdown.Item>
        <Icon name="dropdown" />
        <span className="text">Japan Sport & Touring</span>
        <Dropdown.Menu>
          <Dropdown.Item>Super Sport</Dropdown.Item>
          <Dropdown.Item>Touring</Dropdown.Item>
          <Dropdown.Item>Spareparts</Dropdown.Item>
          <Dropdown.Item>Accesoris</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown.Item>
      <Dropdown.Item>
        <Icon name="dropdown" />
        <span className="text">Japan Retro</span>
        <Dropdown.Menu>
          <Dropdown.Item>Vintage Trail</Dropdown.Item>
          <Dropdown.Item>Bebek Vintage</Dropdown.Item>
          <Dropdown.Item>Spareparts</Dropdown.Item>
          <Dropdown.Item>Accesoris</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown.Item>
      <Dropdown.Item>
        <Icon name="dropdown" />
        <span className="text">Japan Modern</span>
        <Dropdown.Menu>
          <Dropdown.Item>Manual Motorcycle</Dropdown.Item>
          <Dropdown.Item>Matic Scooter</Dropdown.Item>
          <Dropdown.Item>Spareparts</Dropdown.Item>
          <Dropdown.Item>Acessories</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown.Item>
    </Dropdown.Menu>
  </Dropdown>
);

const Headernav = () => <ResponsiveContainer />;
export default Headernav;
