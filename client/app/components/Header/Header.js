import React, { Component } from "react";
import {
  Container,
  Divider,
  Dropdown,
  Grid,
  Header,
  Image,
  List,
  Menu,
  Segment,
  Input,
  Icon,
  DropdownItem,
  Popup,
  Button,
  MenuItem,
  Label,
  GridRow,
  Form,
  GridColumn,
  Checkbox,
  Message
} from "semantic-ui-react";
import { Link } from "react-router-dom";
import { equal } from "assert";
import { getFromStorage, setInStorage } from "../../utils/storage";
import { Route, withRouter } from "react-router-dom";

var Ams = ["Motorcycle", "Sparepart", "Accesoris"];
var Ems = ["European Modern", "European Classic", "European Scooter"];
var Jms = ["Japan Sport & Touring", "Japanese Retro", "Japan Modern"];

const DropdownMotopedia = () => (
  <Dropdown item simple text="Motopedia">
    <Dropdown.Menu>
      <Dropdown.Item>MotoArticle</Dropdown.Item>
      <Dropdown.Item>News & Event</Dropdown.Item>
    </Dropdown.Menu>
  </Dropdown>
);

const PopupMotopedia = () => (
  <Popup trigger={<MenuItem>Motopedia</MenuItem>} flowing>
    <Grid centered divided columns={3}>
      <Grid.Column textAlign="center">
        <Header as="h5">Japan Sport & Touring</Header>
        <List divided selection>
          <List.Item>Super Sport</List.Item>
          <List.Item>Touring</List.Item>
          <List.Item>Spareparts</List.Item>
          <List.Item>Acessories</List.Item>
        </List>
      </Grid.Column>
      <Grid.Column textAlign="center">
        <Header as="h5">Japanese Retro</Header>
        <List divided selection>
          <List.Item>Motorcycle Retro</List.Item>
          <List.Item>Vintage trail</List.Item>
          <List.Item>Spareparts</List.Item>
          <List.Item>Acessories</List.Item>
        </List>
      </Grid.Column>
      <Grid.Column textAlign="center">
        <Header as="h5">Japan Modern</Header>
        <List divided selection>
          <List.Item>Manual motorcycle</List.Item>
          <List.Item>scooter matic</List.Item>
          <List.Item>Spareparts</List.Item>
          <List.Item>Acessories</List.Item>
        </List>
      </Grid.Column>
    </Grid>
  </Popup>
);
const DropdownApparel = () => (
  <Dropdown item simple text="Apparel">
    <Dropdown.Menu>
      <Dropdown.Item>Riding Gear</Dropdown.Item>
      <Dropdown.Item>Bikkers Clothing</Dropdown.Item>
      <Dropdown.Item>Bickers Accesoris</Dropdown.Item>
    </Dropdown.Menu>
  </Dropdown>
);
const PopupApparel = () => (
  <Popup trigger={<MenuItem>Apparels</MenuItem>} flowing hideOnScroll>
    <Grid centered divided columns={3}>
      <Grid.Column textAlign="center">
        <Header as="h5">Japan Sport & Touring</Header>
        <List divided selection>
          <List.Item>Super Sport</List.Item>
          <List.Item>Touring</List.Item>
          <List.Item>Spareparts</List.Item>
          <List.Item>Acessories</List.Item>
        </List>
      </Grid.Column>
      <Grid.Column textAlign="center">
        <Header as="h5">Japanese Retro</Header>
        <List divided selection>
          <List.Item>Motorcycle Retro</List.Item>
          <List.Item>Vintage trail</List.Item>
          <List.Item>Spareparts</List.Item>
          <List.Item>Acessories</List.Item>
        </List>
      </Grid.Column>
      <Grid.Column textAlign="center">
        <Header as="h5">Japan Modern</Header>
        <List divided selection>
          <List.Item>Manual motorcycle</List.Item>
          <List.Item>scooter matic</List.Item>
          <List.Item>Spareparts</List.Item>
          <List.Item>Acessories</List.Item>
        </List>
      </Grid.Column>
    </Grid>
  </Popup>
);

const DropdownProfile = () => (
  <Dropdown item simple text="Business Profile">
    <Dropdown.Menu>
      <Dropdown.Item>Builder</Dropdown.Item>
      <Dropdown.Item>Service Station</Dropdown.Item>
      <Dropdown.Item>Paint Job</Dropdown.Item>
    </Dropdown.Menu>
  </Dropdown>
);
const PopupBusinesProfile = () => (
  <Popup trigger={<MenuItem>Business Profile</MenuItem>} flowing hideOnScroll>
    <Grid centered divided columns={3}>
      <Grid.Column textAlign="center">
        <Header as="h5">Japan Sport & Touring</Header>
        <List divided selection>
          <List.Item>Super Sport</List.Item>
          <List.Item>Touring</List.Item>
          <List.Item>Spareparts</List.Item>
          <List.Item>Acessories</List.Item>
        </List>
      </Grid.Column>
      <Grid.Column textAlign="center">
        <Header as="h5">Japanese Retro</Header>
        <List divided selection>
          <List.Item>Motorcycle Retro</List.Item>
          <List.Item>Vintage trail</List.Item>
          <List.Item>Spareparts</List.Item>
          <List.Item>Acessories</List.Item>
        </List>
      </Grid.Column>
      <Grid.Column textAlign="center">
        <Header as="h5">Japan Modern</Header>
        <List divided selection>
          <List.Item>Manual motorcycle</List.Item>
          <List.Item>scooter matic</List.Item>
          <List.Item>Spareparts</List.Item>
          <List.Item>Acessories</List.Item>
        </List>
      </Grid.Column>
    </Grid>
  </Popup>
);
const DropdownJapan = () => (
  <Dropdown item simple text="Japanese Motorcycle">
    <Dropdown.Menu>
      <Dropdown.Item>
        <Icon name="dropdown" />
        <span className="text">Japan Sport & Touring</span>
        <Dropdown.Menu>
          <Dropdown.Item>Super Sport</Dropdown.Item>
          <Dropdown.Item>Touring</Dropdown.Item>
          <Dropdown.Item>Spareparts</Dropdown.Item>
          <Dropdown.Item>Accesoris</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown.Item>
      <Dropdown.Item>
        <Icon name="dropdown" />
        <span className="text">Japan Retro</span>
        <Dropdown.Menu>
          <Dropdown.Item>Vintage Trail</Dropdown.Item>
          <Dropdown.Item>Bebek Vintage</Dropdown.Item>
          <Dropdown.Item>Spareparts</Dropdown.Item>
          <Dropdown.Item>Accesoris</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown.Item>
      <Dropdown.Item>
        <Icon name="dropdown" />
        <span className="text">Japan Modern</span>
        <Dropdown.Menu>
          <Dropdown.Item>Manual Motorcycle</Dropdown.Item>
          <Dropdown.Item>Matic Scooter</Dropdown.Item>
          <Dropdown.Item>Spareparts</Dropdown.Item>
          <Dropdown.Item>Acessories</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown.Item>
    </Dropdown.Menu>
  </Dropdown>
);
const PopupMenuJapan = () => (
  <Popup trigger={<MenuItem>Japanese Motorcycle</MenuItem>} flowing hoverable>
    <Grid centered divided columns={3}>
      <Grid.Column textAlign="center">
        <Header as="h5">Japan Sport & Touring</Header>
        <List divided selection>
          <List.Item>Super Sport</List.Item>
          <List.Item>Touring</List.Item>
          <List.Item>Spareparts</List.Item>
          <List.Item>Acessories</List.Item>
        </List>
      </Grid.Column>
      <Grid.Column textAlign="center">
        <Header as="h5">Japanese Retro</Header>
        <List divided selection>
          <List.Item>Motorcycle Retro</List.Item>
          <List.Item>Vintage trail</List.Item>
          <List.Item>Spareparts</List.Item>
          <List.Item>Acessories</List.Item>
        </List>
      </Grid.Column>
      <Grid.Column textAlign="center">
        <Header as="h5">Japan Modern</Header>
        <List divided selection>
          <List.Item>Manual motorcycle</List.Item>
          <List.Item>scooter matic</List.Item>
          <List.Item>Spareparts</List.Item>
          <List.Item>Acessories</List.Item>
        </List>
      </Grid.Column>
    </Grid>
  </Popup>
);
const DropdownEuropean = () => (
  <Dropdown item simple text="European Motorcycle">
    <Dropdown.Menu>
      <Dropdown.Item>
        <Icon name="dropdown" />
        <span className="text">Modern European</span>
        <Dropdown.Menu>
          <Dropdown.Item>Motorcycle</Dropdown.Item>
          <Dropdown.Item>Spareparts</Dropdown.Item>
          <Dropdown.Item>Accesoris</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown.Item>
      <Dropdown.Item>
        <Icon name="dropdown" />
        <span className="text">Classic European</span>
        <Dropdown.Menu>
          <Dropdown.Item>Motorcycle</Dropdown.Item>
          <Dropdown.Item>Spareparts</Dropdown.Item>
          <Dropdown.Item>Accesoris</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown.Item>
      <Dropdown.Item>
        <Icon name="dropdown" />
        <span className="text">European Scooter</span>
        <Dropdown.Menu>
          <Dropdown.Item>Scooter</Dropdown.Item>
          <Dropdown.Item>Spareparts</Dropdown.Item>
          <Dropdown.Item>Acessories</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown.Item>
    </Dropdown.Menu>
  </Dropdown>
);
const PopupMenuEuropean = () => (
  <Popup
    trigger={<MenuItem>European Motorcycle</MenuItem>}
    flowing
    hideOnScroll
  >
    <Grid centered divided columns={3}>
      <Grid.Column textAlign="center">
        <Header as="h5">European Modern</Header>
        <List divided selection>
          <List.Item>Motorcycle</List.Item>
          <List.Item>Spareparts</List.Item>
          <List.Item>Accesoris</List.Item>
        </List>
      </Grid.Column>
      <Grid.Column textAlign="center">
        <Header as="h5">European Classic</Header>
        <List divided selection>
          <List.Item>Motorcycle</List.Item>
          <List.Item>Spareparts</List.Item>
          <List.Item>Accesoris</List.Item>
        </List>
      </Grid.Column>
      <Grid.Column textAlign="center">
        <Header as="h5">European Scooter</Header>
        <List divided selection>
          <List.Item>Motorcycle</List.Item>
          <List.Item>Spareparts</List.Item>
          <List.Item>Accesoris</List.Item>
        </List>
      </Grid.Column>
    </Grid>
  </Popup>
);
const DropdownAmerican = () => (
  <Dropdown item simple text="American Motorcycle">
    <Dropdown.Menu>
      <Dropdown.Item>
        <a className="text" style={{ color: "black" }} href="/users" />
        <span className="text">Motorcycle</span>
      </Dropdown.Item>
      <Dropdown.Item>
        <a className="text" style={{ color: "black" }} href="/users">
          Spareparts
        </a>
      </Dropdown.Item>
      <Dropdown.Item>
        <a className="text" style={{ color: "black" }} href="/users" />
        <span className="text">Accesoris</span>
      </Dropdown.Item>
    </Dropdown.Menu>
  </Dropdown>
);

const PopupMenuAmerican = () => (
  <Popup
    trigger={<MenuItem>American Motorcycle</MenuItem>}
    flowing
    hideOnScroll
  >
    <Grid centered divided columns={3}>
      <Grid.Column textAlign="center">
        <Header as="h5">Motorcycle</Header>
        <List divided selection>
          <List.Item>
            <Link to="/helloworld?test=test">Harley Davison</Link>
          </List.Item>
          <List.Item>Indian</List.Item>
          <List.Item>Polaris</List.Item>
          <List.Item>Victory</List.Item>
          <List.Item>Buell</List.Item>
          <List.Item>Other</List.Item>
        </List>
      </Grid.Column>
      <Grid.Column textAlign="center">
        <Header as="h5">Spareparts</Header>
        <List divided selection>
          <List.Item>Harley Davison</List.Item>
          <List.Item>Indian</List.Item>
          <List.Item>Polaris</List.Item>
          <List.Item>Victory</List.Item>
          <List.Item>Buell</List.Item>
          <List.Item>Other</List.Item>
        </List>
      </Grid.Column>
      <Grid.Column textAlign="center">
        <Header as="h5">Acessories</Header>
        <List divided selection>
          <List.Item>Harley Davison</List.Item>
          <List.Item>Indian</List.Item>
          <List.Item>Polaris</List.Item>
          <List.Item>Victory</List.Item>
          <List.Item>Buell</List.Item>
          <List.Item>Other</List.Item>
        </List>
      </Grid.Column>
    </Grid>
  </Popup>
);

//const Headernav = () => (
class Headernav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      error: false,
      errEmail: false,
      errPassword: false,
      popupOpen: false,
      token: "",
      searchTxt: ""
    };
    this.onTextBoxChangePassword = this.onTextBoxChangePassword.bind(this);
    this.onTextBoxChangeEmail = this.onTextBoxChangeEmail.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onLogout = this.onLogout.bind(this);
    this.handleSearchClick = this.handleSearchClick.bind(this);
    this.onSearchTxtChange = this.onSearchTxtChange.bind(this);
  }
  componentDidMount() {
    const obj = getFromStorage("the_main_app");

    if (obj && obj.token) {
      const { token } = obj;
      // Verify token
      fetch("/api/account/verify?token=" + token)
        .then(res => res.json())
        .then(json => {
          console.log(json);
          if (json.success) {
            this.setState({
              token,
              isLoading: false
            });
          } else {
            this.setState({
              isLoading: false
            });
          }
        });
    }
  }
  handleSearchClick(event) {
    const { searchTxt } = this.state;
    //alert("SearchTxt :" + searchTxt);
    fetch("api/search/", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        searchTxt: searchTxt
      })
    })
      .then(res => res.json())
      .then(json => {
        console.log("json", json);
        if (json.success) {
          this.setState({
            signUpError: json.message
            // isLoading: false,
            // signUpEmail: "",
            // signUpPassword: ""
          });
          this.props.history.push("/");
        } else {
          this.setState({
            signUpError: json.message
            //isLoading: false
          });
        }
      });
  }
  onSearchTxtChange(event) {
    this.setState({
      searchTxt: event.target.value
    });
  }
  onTextBoxChangeEmail(event) {
    this.setState({
      email: event.target.value
    });
  }
  onTextBoxChangePassword(event) {
    this.setState({
      password: event.target.value
    });
  }
  onSubmit() {
    // Grab state
    const { email, password } = this.state;

    //close popup

    // Post request to backend
    fetch("/api/account/signin", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
      .then(res => res.json())
      .then(json => {
        console.log("json", json);
        if (json.success) {
          setInStorage("the_main_app", { token: json.token });
          this.setState({
            signInError: json.message,
            isLoading: false,
            signInPassword: "",
            signInEmail: "",
            token: json.token
          });
          this.props.history.push("/");
        } else {
          this.setState({
            signInError: json.message,
            error: json.message,
            isLoading: false
          });
        }
      });
  }
  onLogout() {
    const obj = getFromStorage("the_main_app");
    if (obj && obj.token) {
      const { token } = obj;
      // Verify token
      fetch("/api/account/logout?token=" + token)
        .then(res => res.json())
        .then(json => {
          if (json.success) {
            this.setState({
              token: "",
              isLoading: false
            });
            this.props.history.push("/");
          } else {
            this.setState({
              isLoading: false
            });
          }
        });
    }
  }
  render() {
    const {
      email,
      password,
      error,
      signInError,
      token,
      searchTxt
    } = this.state;
    // e.preventDefault();
    //let error = false;
    let popupShow = "";
    if (!token) {
      popupShow = (
        <Popup
          trigger={
            <Button
              color="green"
              icon="user circle outline"
              content="Signin/up"
            />
          }
          content={
            <Grid centered>
              <Grid.Column textAlign="center">
                {error ? (
                  <Message header="Signin Error" content={signInError} />
                ) : null}

                <Form>
                  <Form.Group>
                    <Form.Field>
                      <input
                        size="50"
                        placeholder="Email Address"
                        onChange={this.onTextBoxChangeEmail}
                      />
                    </Form.Field>
                  </Form.Group>
                  <Form.Group>
                    <Form.Field>
                      <input
                        size="50"
                        placeholder="Password"
                        type="password"
                        onChange={this.onTextBoxChangePassword}
                      />
                    </Form.Field>
                  </Form.Group>

                  <Grid columns={2} divided>
                    <GridRow>
                      <GridColumn>
                        <Checkbox label="Remember me?" />
                      </GridColumn>
                      <GridColumn>
                        <Link to="/">Forgot password?</Link>
                      </GridColumn>
                    </GridRow>
                    <GridRow>
                      <Button
                        content="Sign In"
                        fluid
                        secondary
                        onClick={this.onSubmit}
                      />
                    </GridRow>
                  </Grid>
                </Form>
                <Grid>
                  <GridColumn>
                    <Link to="/signup">Dont have account? signup</Link>
                  </GridColumn>
                </Grid>
              </Grid.Column>
            </Grid>
          }
          on="click"
          closeOnMouseLeave={true}
          wide
        />
      );
    } else {
      popupShow = (
        <Popup
          trigger={
            <Button
              color="green"
              icon="user circle outline"
              content="Account"
            />
          }
          content={
            <Grid centered>
              <Grid.Column textAlign="center">
                <GridRow>
                  <strong>Welcome, Member</strong>
                </GridRow>
                <GridRow>
                  <Button
                    content="Sign Out"
                    fluid
                    secondary
                    onClick={this.onLogout}
                  />
                </GridRow>
              </Grid.Column>
            </Grid>
          }
          on="click"
          closeOnMouseLeave={true}
          wide
        />
      );
    }
    return (
      <Menu Menu fixed="top" color="white" inverted>
        <Menu.Menu position="left">
          <Menu.Item as="a" header>
            <Link to="/">
              <Image
                size="small"
                src="/assets/img/motostation_logo.png"
                style={{ marginRight: "1.5em" }}
              />
            </Link>
          </Menu.Item>
          <Menu.Item>
            <Form>
              <Input
                size="medium"
                icon={
                  <Icon
                    name="search"
                    inverted
                    circular
                    link
                    onClick={this.handleSearchClick}
                  />
                }
                placeholder="Search..."
                onChange={this.onSearchTxtChange}
              />
            </Form>
          </Menu.Item>
          <DropdownAmerican />

          <DropdownEuropean />

          <DropdownJapan />
        </Menu.Menu>
        <Menu.Menu position="right">
          <Menu.Item as="a">
            <Link to="/test/dynamicselect">
              <Icon name="grid layout" />
              <strong>Sell Items</strong>
            </Link>
          </Menu.Item>

          <Menu.Item as="a" link>
            <Icon name="comment alternate outline" />
            <strong>Notification</strong>
          </Menu.Item>
          {popupShow}
          <Menu.Item as="a" link>
            <Icon name="user circle outline" />
            Cart
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    );
  }
}
//);

/*
 style={{ width: "350px", maxLengt: "3" }}

<Menu.Item as="a" link>
        <Link to="/signup">
          <Icon name="user circle outline" />
          Sign In/Up
        </Link>
      </Menu.Item>


import { Link } from 'react-router-dom';

const Header = () => (
  <header>
    <Link to="/">Home</Link>

    <nav>
      <Link to="/helloworld">Hello World</Link>
    </nav>

    <hr />
  </header>
);
*/
//export default Headernav;
export default withRouter(Headernav);
