import React, { Component } from "react";
import {
  Container,
  Divider,
  Dropdown,
  Grid,
  Header,
  Image,
  List,
  Menu,
  Segment,
  Input,
  Icon,
  DropdownItem,
  Popup,
  Button,
  MenuItem,
  Label,
  GridRow,
  Form,
  GridColumn,
  Checkbox,
  Message
} from "semantic-ui-react";
import { Link } from "react-router-dom";
import { getFromStorage, setInStorage } from "../../utils/storage";
import { Route, withRouter } from "react-router-dom";

const DropdownAmerican = () => (
  <Dropdown item simple text="American Motorcycle">
    <Dropdown.Menu>
      <Dropdown.Item>
        <Icon name="dropdown" />
        <span className="text">Motorcycle</span>
        <Dropdown.Menu>
          <Dropdown.Item>
            <a style={{ color: "black" }} href="/users">
              Harley Davison
            </a>
          </Dropdown.Item>
          <Dropdown.Item>Indian</Dropdown.Item>
          <Dropdown.Item>Polaris</Dropdown.Item>
          <Dropdown.Item>Victory</Dropdown.Item>
          <Dropdown.Item>Buell</Dropdown.Item>
          <Dropdown.Item>Other</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown.Item>
      <Dropdown.Item>
        <Icon name="dropdown" />
        <span className="text">Spareparts</span>
        <Dropdown.Menu>
          <Dropdown.Item>Harley Davison</Dropdown.Item>
          <Dropdown.Item>Indian</Dropdown.Item>
          <Dropdown.Item>Polaris</Dropdown.Item>
          <Dropdown.Item>Victory</Dropdown.Item>
          <Dropdown.Item>Buell</Dropdown.Item>
          <Dropdown.Item>Other</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown.Item>
      <Dropdown.Item>
        <Icon name="dropdown" />
        <span className="text">Accesoris</span>
        <Dropdown.Menu>
          <Dropdown.Item>Harley Davison</Dropdown.Item>
          <Dropdown.Item>Indian</Dropdown.Item>
          <Dropdown.Item>Polaris</Dropdown.Item>
          <Dropdown.Item>Victory</Dropdown.Item>
          <Dropdown.Item>Buell</Dropdown.Item>
          <Dropdown.Item>Other</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown.Item>
    </Dropdown.Menu>
  </Dropdown>
);

class Headernav extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {}
  render() {}
}
export default withRouter(Headernav);
