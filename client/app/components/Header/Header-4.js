import React, { Component } from "react";

import {
  Header,
  Image,
  Form,
  Input,
  Icon,
  Dropdown,
  Popup,
  Button,
  Grid,
  GridRow,
  GridColumn,
  Checkbox,
  Message
} from "semantic-ui-react";
import "./Header-4.css";
import { Link } from "react-router-dom";
import { getFromStorage, setInStorage } from "../../utils/storage";
import { Route, withRouter } from "react-router-dom";
import { list } from "../Page/Product/api-product";
var util = require("util");

const DropdownAmerican = props => (
  <Dropdown item simple text="American Motorcycle">
    <Dropdown.Menu>
      <Dropdown.Item>
        <Link
          className="textlink"
          to={{
            pathname: "/produk/search/",
            state: { searchKey: "American Motorcycle" }
          }}
        >
          <span className="text">Motorcycle</span>
        </Link>
      </Dropdown.Item>
      <Dropdown.Item>
        <a className="text" style={{ color: "black" }} href="/users">
          Spareparts
        </a>
      </Dropdown.Item>
      <Dropdown.Item>
        <a className="text" style={{ color: "black" }} href="/users" />
        <span className="text">Accesoris</span>
      </Dropdown.Item>
    </Dropdown.Menu>
  </Dropdown>
);
const DropdownEuropean = () => (
  <Dropdown item simple text="European Motorcycle">
    <Dropdown.Menu>
      <Dropdown.Item>
        <Icon name="dropdown" />
        <span className="text">Modern European</span>
        <Dropdown.Menu>
          <Dropdown.Item>
            <Link className="textlink" to="/signup">
              Motorcycle
            </Link>
          </Dropdown.Item>
          <Dropdown.Item>Spareparts</Dropdown.Item>
          <Dropdown.Item>Accesoris</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown.Item>
      <Dropdown.Item>
        <Icon name="dropdown" />
        <span className="text">Classic European</span>
        <Dropdown.Menu>
          <Dropdown.Item>Motorcycle</Dropdown.Item>
          <Dropdown.Item>Spareparts</Dropdown.Item>
          <Dropdown.Item>Accesoris</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown.Item>
      <Dropdown.Item>
        <Icon name="dropdown" />
        <span className="text">European Scooter</span>
        <Dropdown.Menu>
          <Dropdown.Item>Scooter</Dropdown.Item>
          <Dropdown.Item>Spareparts</Dropdown.Item>
          <Dropdown.Item>Acessories</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown.Item>
    </Dropdown.Menu>
  </Dropdown>
);
const DropdownJapan = () => (
  <Dropdown item simple text="Japanese Motorcycle">
    <Dropdown.Menu>
      <Dropdown.Item>
        <Icon name="dropdown" />
        <span className="text">Japan Sport & Touring</span>
        <Dropdown.Menu>
          <Dropdown.Item>Super Sport</Dropdown.Item>
          <Dropdown.Item>Touring</Dropdown.Item>
          <Dropdown.Item>Spareparts</Dropdown.Item>
          <Dropdown.Item>Accesoris</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown.Item>
      <Dropdown.Item>
        <Icon name="dropdown" />
        <span className="text">Japan Retro</span>
        <Dropdown.Menu>
          <Dropdown.Item>Vintage Trail</Dropdown.Item>
          <Dropdown.Item>Bebek Vintage</Dropdown.Item>
          <Dropdown.Item>Spareparts</Dropdown.Item>
          <Dropdown.Item>Accesoris</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown.Item>
      <Dropdown.Item>
        <Icon name="dropdown" />
        <span className="text">Japan Modern</span>
        <Dropdown.Menu>
          <Dropdown.Item>Manual Motorcycle</Dropdown.Item>
          <Dropdown.Item>Matic Scooter</Dropdown.Item>
          <Dropdown.Item>Spareparts</Dropdown.Item>
          <Dropdown.Item>Acessories</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown.Item>
    </Dropdown.Menu>
  </Dropdown>
);

class Toolbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      error: false,
      errEmail: false,
      errPassword: false,
      popupOpen: false,
      token: "",
      searchTxt: ""
    };
    this.onTextBoxChangePassword = this.onTextBoxChangePassword.bind(this);
    this.onTextBoxChangeEmail = this.onTextBoxChangeEmail.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onLogout = this.onLogout.bind(this);
    this.handleSearchClick = this.handleSearchClick.bind(this);
    this.onSearchTxtChange = this.onSearchTxtChange.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
    // this.handleDeleteClick = this.handleDeleteClick.bind(this);
  }
  componentDidMount() {
    const obj = getFromStorage("the_main_app");

    if (obj && obj.token) {
      const { token } = obj;
      // Verify token
      fetch("/api/account/verify?token=" + token)
        .then(res => res.json())
        .then(json => {
          console.log(json);
          if (json.success) {
            this.setState({
              token,
              isLoading: false
            });
          } else {
            this.setState({
              isLoading: false
            });
          }
        });
    }
  }
  onLinkClick() {
    alert("Click");
  }
  onKeyPress(e) {
    if (e.which === 13) {
      if (window.location.pathname === "/produk/search") {
        window.location.reload();
      }
      const { searchTxt } = this.state;
      this.props.history.push({
        pathname: "/produk/search",
        state: { searchKey: searchTxt }
      });
    }
  }
  handleSearchClick(event) {
    if (window.location.pathname === "/produk/search") {
      window.location.reload();
      //this.props.loadProducts;
    }
    const { searchTxt } = this.state;
    this.props.history.push({
      pathname: "/produk/search",
      state: { searchKey: searchTxt }
    });
  }

  onSearchTxtChange(event) {
    this.setState({
      searchTxt: event.target.value
    });
  }
  onTextBoxChangeEmail(event) {
    this.setState({
      email: event.target.value
    });
  }
  onTextBoxChangePassword(event) {
    this.setState({
      password: event.target.value
    });
  }
  onSubmit() {
    // Grab state
    const { email, password } = this.state;

    //close popup

    // Post request to backend
    fetch("/api/account/signin", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
      .then(res => res.json())
      .then(json => {
        console.log("json", json);
        if (json.success) {
          setInStorage("the_main_app", { token: json.token });
          this.setState({
            signInError: json.message,
            isLoading: false,
            signInPassword: "",
            signInEmail: "",
            token: json.token
          });
          this.props.history.push("/");
        } else {
          this.setState({
            signInError: json.message,
            error: json.message,
            isLoading: false
          });
        }
      });
  }
  onLogout() {
    const obj = getFromStorage("the_main_app");
    if (obj && obj.token) {
      const { token } = obj;
      // Verify token
      fetch("/api/account/logout?token=" + token)
        .then(res => res.json())
        .then(json => {
          if (json.success) {
            this.setState({
              token: "",
              isLoading: false
            });
            this.props.history.push("/");
          } else {
            this.setState({
              isLoading: false
            });
          }
        });
    }
  }

  onKeyPress(e) {
    if (e.which === 13) {
      //alert(window.location.pathname);
      if (window.location.pathname === "/produk/search") {
        // alert("panggil loadProducts");
        window.location.reload();
        //this.props.loadProducts;
      }
      const { searchTxt } = this.state;
      this.props.history.push({
        pathname: "/produk/search",
        state: { searchKey: searchTxt }
      });

      // this.handleSendMessage();
    }
  }
  handleDeleteClick() {
    this.handleSearchClick();
  }

  render() {
    const {
      email,
      password,
      error,
      signInError,
      token,
      searchTxt
    } = this.state;
    let popupShow = "";
    if (!token) {
      popupShow = (
        <Popup
          trigger={
            <Button
              color="green"
              icon="user circle outline"
              content="Signin/up"
            />
          }
          content={
            <Grid centered>
              <Grid.Column textAlign="center">
                {error ? (
                  <Message header="Signin Error" content={signInError} />
                ) : null}

                <Form>
                  <Form.Group>
                    <Form.Field>
                      <input
                        size="50"
                        placeholder="Email Address"
                        onChange={this.onTextBoxChangeEmail}
                      />
                    </Form.Field>
                  </Form.Group>
                  <Form.Group>
                    <Form.Field>
                      <input
                        size="50"
                        placeholder="Password"
                        type="password"
                        onChange={this.onTextBoxChangePassword}
                      />
                    </Form.Field>
                  </Form.Group>

                  <Grid columns={2} divided>
                    <GridRow>
                      <GridColumn>
                        <Checkbox label="Remember me?" />
                      </GridColumn>
                      <GridColumn>
                        <Link to="/">Forgot password?</Link>
                      </GridColumn>
                    </GridRow>
                    <GridRow>
                      <Button
                        content="Sign In"
                        fluid
                        secondary
                        onClick={this.onSubmit}
                      />
                    </GridRow>
                  </Grid>
                </Form>
                <Grid>
                  <GridColumn>
                    <Link to="/signup">Dont have account? signup</Link>
                  </GridColumn>
                </Grid>
              </Grid.Column>
            </Grid>
          }
          on="click"
          hideOnScroll
          wide
        />
      );
    } else {
      popupShow = (
        <Popup
          trigger={
            <Button
              color="green"
              icon="user circle outline"
              content="Account"
            />
          }
          content={
            <Grid centered>
              <Grid.Column textAlign="center">
                <GridRow>
                  <strong>Welcome, Member</strong>
                </GridRow>
                <GridRow>
                  <Link to={"/user/profile/" + this.state.token}>
                    <strong>My Profile</strong>
                  </Link>
                </GridRow>
                <GridRow>
                  <Button
                    content="Sign Out"
                    fluid
                    secondary
                    onClick={this.onLogout}
                  />
                </GridRow>
              </Grid.Column>
            </Grid>
          }
          on="click"
          closeOnMouseLeave={true}
          wide
        />
      );
    }
    return (
      <div>
        <ul class="toolbarnav" role="navigation">
          <li>
            <a href="/">
              <Image size="medium" src="/assets/img/motostation_logo.png" />
            </a>
          </li>
          <li className="search_bar">
            <Form>
              <Input
                className="inputsearch"
                icon={
                  <Icon
                    name="search"
                    inverted
                    circular
                    link
                    onClick={this.handleSearchClick}
                  />
                }
                placeholder="Search..."
                onKeyPress={this.onKeyPress}
                onChange={this.onSearchTxtChange}
              />
            </Form>
            <ul>
              <li>
                <DropdownAmerican token={this.state.token} />
              </li>
              <li>
                <DropdownEuropean />
              </li>
              <li>
                <DropdownJapan />
              </li>
            </ul>
          </li>
          <li>
            <Link to="/test/dynamicselect">
              <Icon name="comment alternate outline" />
              Notifications
            </Link>
          </li>

          <li>
            <Link to={"/produk/new/" + this.state.token}>
              <Icon name="grid layout" />
              Sell&nbsp;Item
            </Link>
          </li>
          <li>
            <Link to="/test/dynamicselect">
              <Icon name="user circle outline" />
              Cart
            </Link>
          </li>

          <li>{popupShow}</li>
        </ul>
      </div>
    );

    /*
<ul class="toolbarnav2" role="navigation">
      <li />
      <li className="search_bar" />
      <li />
    </ul>
*/
    const Toolbarx = props => (
      <Header className="toolbar">
        <nav className="toolbar_navigation">
          <div />
          <div className="toolbar_logo">
            <a href="/">
              <Image size="small" src="/assets/img/motostation_logo.png" />
            </a>
          </div>

          <div className="toolbar_navigation_item">
            <ul>
              <li className="search_bar">
                <Form>
                  <Input
                    icon={<Icon name="search" inverted circular link />}
                    placeholder="Search..."
                  />
                </Form>
              </li>
              <li>
                <a href="">Sell Item</a>
              </li>
              <li>
                <a href="">Notification</a>
              </li>
              <li>
                <a href="">Cart</a>
              </li>
            </ul>
          </div>
        </nav>
      </Header>
    );
  }
}

export default withRouter(Toolbar);
