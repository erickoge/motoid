//import "react-responsive-carousel/lib/styles/carousel.min.css";
import React, { Component } from "react";
import ReactDOM from "react-dom";
import { Carousel } from "react-responsive-carousel";

const Mscarousel = () => (
  <div>
    <Carousel
      showThumbs={false}
      autoPlay={true}
      showArrows={false}
      showStatus={false}
      showIndicators={false}
      infiniteLoop={true}
    >
      <div>
        <img src="assets/img/11.jpg" />
      </div>
      <div>
        <img src="assets/img/21.jpg" />
      </div>
      <div>
        <img src="assets/img/31.jpg" />
      </div>
    </Carousel>
  </div>
);
export default Mscarousel;
