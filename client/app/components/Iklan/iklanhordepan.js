import React from "react";
import { Grid, Advertisement } from "semantic-ui-react";

const Iklanhordepan = () => (
  <Grid columns="equal">
    <Grid.Column />
    <Grid.Column width={12}>
      <Grid>
        <Grid.Column width={16}>
          <Advertisement unit="billboard" test="Iklan Horizontal Depan" />
        </Grid.Column>
      </Grid>
    </Grid.Column>
    <Grid.Column />
  </Grid>
);
export default Iklanhordepan;
