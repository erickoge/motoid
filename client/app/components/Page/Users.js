import React, { Component } from "react";
import {
  Input,
  Label,
  Menu,
  Grid,
  Segment,
  Image,
  GridColumn,
  GridRow,
  Header,
  Center,
  MenuHeader,
  List
} from "semantic-ui-react";
import { Redirect } from "react-router";

export default class Users extends Component {
  state = { activeItem: "inbox", navigate: false };

  handleItemClick = (e, { name }) => {
    this.setState({ activeItem: name, navigate: true });
    // return <Redirect to="/product" push={true} />;
  };
  render() {
    const { activeItem, navigate } = this.state;
    if (navigate) {
      if (activeItem === "Profile") {
        return <Redirect to="/profile" push={true} />;
      }
      if (activeItem === "Edit") {
        return <Redirect to="/edit" push={true} />;
      }
      //console.log("-" + activeItem + "-");
    }
    return (
      <div style={{ marginTop: "10.3em" }}>
        <Grid columns="equal">
          <Grid.Column />
          <Grid.Column width={10}>
            <Grid>
              <GridColumn width={4}>
                <Menu size="large" vertical>
                  <MenuHeader>
                    <Header as="h2" icon textAlign="center">
                      <i className="huge circular user link icon center" />
                    </Header>
                    <Header as="h3" icon textAlign="center">
                      Erick Hadianto
                    </Header>
                    <Header as="h5" icon textAlign="center">
                      Join At July 2018{" "}
                    </Header>
                  </MenuHeader>
                  <Menu.Item
                    name="Profile"
                    active={activeItem === "Profile"}
                    onClick={this.handleItemClick}
                  >
                    Profile
                  </Menu.Item>

                  <Menu.Item
                    name="Edit"
                    active={activeItem === "Edit"}
                    onClick={this.handleItemClick}
                  >
                    Edit Profile
                  </Menu.Item>

                  <Menu.Item
                    name="watch"
                    active={activeItem === "watch"}
                    onClick={this.handleItemClick}
                  >
                    <Label color="teal">1</Label>
                    Watch List
                  </Menu.Item>

                  <Menu.Item
                    name="sale"
                    active={activeItem === "sale"}
                    onClick={this.handleItemClick}
                  >
                    <Label color="red">21</Label>
                    Items For Sell
                  </Menu.Item>
                  <Menu.Item
                    name="password"
                    active={activeItem === "password"}
                    onClick={this.handleItemClick}
                  >
                    Change Password
                  </Menu.Item>
                </Menu>
              </GridColumn>
              <GridColumn width={12}>
                <Segment>
                  <Header as="h3">My Profile</Header>
                </Segment>
                <Grid columns="equal">
                  <GridColumn>
                    <List divided relaxed>
                      <List.Item>
                        <List.Icon
                          name="user"
                          size="large"
                          verticalAlign="middle"
                        />
                        <List.Content>
                          <List.Header as="a">First Name</List.Header>
                          <List.Description as="a">Erick</List.Description>
                        </List.Content>
                      </List.Item>
                      <List.Item>
                        <List.Icon
                          name="github"
                          size="large"
                          verticalAlign="middle"
                        />
                        <List.Content>
                          <List.Header as="a">Gender </List.Header>
                          <List.Description as="a">Male </List.Description>
                        </List.Content>
                      </List.Item>
                      <List.Item>
                        <List.Icon
                          name="github"
                          size="large"
                          verticalAlign="middle"
                        />
                        <List.Content>
                          <List.Header as="a">Place Of Birth </List.Header>
                          <List.Description as="a">Bandung </List.Description>
                        </List.Content>
                      </List.Item>
                      <List.Item>
                        <List.Icon
                          name="github"
                          size="large"
                          verticalAlign="middle"
                        />
                        <List.Content>
                          <List.Header as="a">Email Address </List.Header>
                          <List.Description as="a">
                            Erickoge@gmail.com{" "}
                          </List.Description>
                        </List.Content>
                      </List.Item>
                    </List>
                  </GridColumn>
                  <GridColumn>
                    <List divided relaxed>
                      <List.Item>
                        <List.Icon
                          name="github"
                          size="large"
                          verticalAlign="middle"
                        />
                        <List.Content>
                          <List.Header as="a">Last Name </List.Header>
                          <List.Description as="a">Hadianto </List.Description>
                        </List.Content>
                      </List.Item>
                      <List.Item>
                        <List.Icon
                          name="github"
                          size="large"
                          verticalAlign="middle"
                        />
                        <List.Content>
                          <List.Header as="a">Role </List.Header>
                          <List.Description as="a">Member </List.Description>
                        </List.Content>
                      </List.Item>
                      <List.Item>
                        <List.Icon
                          name="github"
                          size="large"
                          verticalAlign="middle"
                        />
                        <List.Content>
                          <List.Header as="a">Date Of Birth </List.Header>
                          <List.Description as="a">
                            22/04/1988{" "}
                          </List.Description>
                        </List.Content>
                      </List.Item>
                      <List.Item>
                        <List.Icon
                          name="github"
                          size="large"
                          verticalAlign="middle"
                        />
                        <List.Content>
                          <List.Header as="a">Phone </List.Header>
                          <List.Description as="a">
                            081321703764{" "}
                          </List.Description>
                        </List.Content>
                      </List.Item>
                    </List>
                  </GridColumn>
                </Grid>
              </GridColumn>
            </Grid>
          </Grid.Column>
          <Grid.Column />
        </Grid>
      </div>
    );
  }
}
