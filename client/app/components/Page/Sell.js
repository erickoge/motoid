import React, { Component } from "react";
import {
  Grid,
  GridColumn,
  Segment,
  Accordion,
  List,
  Image
} from "semantic-ui-react";
import { Link } from "react-router-dom";
class Sell extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const Level12Content = (
      <div>
        <List horizontal>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/sellfields?id=1">Harley Davidson</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Indian</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/"> Polaris</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Victory</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Buell</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Others</Link>
              </List.Header>
            </List.Content>
          </List.Item>
        </List>
      </div>
    );
    const Level13Content = (
      <div>
        <List horizontal>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Harley Davidson</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Indian</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/"> Polaris</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Victory</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Buell</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Others</Link>
              </List.Header>
            </List.Content>
          </List.Item>
        </List>
      </div>
    );
    const Level14Content = (
      <div>
        <List horizontal>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Harley Davidson</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Indian</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/"> Polaris</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Victory</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Buell</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Others</Link>
              </List.Header>
            </List.Content>
          </List.Item>
        </List>
      </div>
    );
    // european
    const Level22Content = (
      <div>
        <List horizontal>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Motorcycle</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Sparepart</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/"> Accesories</Link>
              </List.Header>
            </List.Content>
          </List.Item>
        </List>
      </div>
    );
    const Level23Content = (
      <div>
        <List horizontal>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Motorcycle</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Sparepart</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/"> Accesories</Link>
              </List.Header>
            </List.Content>
          </List.Item>
        </List>
      </div>
    );
    const Level24Content = (
      <div>
        <List horizontal>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Motorcycle</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Sparepart</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/"> Accesories</Link>
              </List.Header>
            </List.Content>
          </List.Item>
        </List>
      </div>
    );
    //japan
    const Level32Content = (
      <div>
        <List horizontal>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Super Sport</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Touring</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Sparepart</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/"> Accesories</Link>
              </List.Header>
            </List.Content>
          </List.Item>
        </List>
      </div>
    );
    const Level33Content = (
      <div>
        <List horizontal>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Vintage Trail</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Bebek Vintage</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Sparepart</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/"> Accesories</Link>
              </List.Header>
            </List.Content>
          </List.Item>
        </List>
      </div>
    );
    const Level34Content = (
      <div>
        <List horizontal>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Manual Motorcycle</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Matic Scooter</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/">Sparepart</Link>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Link to="/"> Accesories</Link>
              </List.Header>
            </List.Content>
          </List.Item>
        </List>
      </div>
    );
    const level1Panels = [
      {
        key: "panel-1a",
        title: "Motorcycle",
        content: { content: Level12Content }
      },
      {
        key: "panel-1b",
        title: "Sparepart",
        content: { content: Level13Content }
      },
      {
        key: "panel-1c",
        title: "Accesories",
        content: { content: Level14Content }
      }
    ];

    const Level1Content = (
      <div>
        <Accordion.Accordion panels={level1Panels} />
      </div>
    );

    const level2Panels = [
      {
        key: "panel-2a",
        title: "Modern European",
        content: { content: Level22Content }
      },
      {
        key: "panel-2b",
        title: "Classic European",
        content: { content: Level23Content }
      },
      {
        key: "panel-2c",
        title: "European Scooter",
        content: { content: Level24Content }
      }
    ];
    const level3Panels = [
      {
        key: "panel-2a",
        title: "Japan Sport & Touring",
        content: { content: Level32Content }
      },
      {
        key: "panel-2b",
        title: "Japan Retro",
        content: { content: Level33Content }
      },
      {
        key: "panel-2c",
        title: "Japan Modern",
        content: { content: Level34Content }
      }
    ];
    const Level2Content = (
      <div>
        <Accordion.Accordion panels={level2Panels} />
      </div>
    );
    const Level3Content = (
      <div>
        <Accordion.Accordion panels={level3Panels} />
      </div>
    );
    const rootPanels = [
      {
        key: "panel-1",
        title: "American Motorcycle",
        content: { content: Level1Content }
      },
      {
        key: "panel-2",
        title: "European Motorcycle",
        content: { content: Level2Content }
      },
      {
        key: "panel-3",
        title: "Japanesse Motorcycle",
        content: { content: Level3Content }
      }
    ];
    return (
      <div style={{ marginTop: "4.3em" }}>
        <Grid columns="equal">
          <GridColumn />
          <GridColumn width={10}>
            <Segment>
              <h4>Sell Items on Motostation</h4>

              <Accordion panels={rootPanels} fluid styled />
            </Segment>
          </GridColumn>
          <GridColumn />
        </Grid>
      </div>
    );
  }
}
export default Sell;
