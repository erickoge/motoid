import React, { Component } from "react";
import Users from "../Users";
import UserMenu from "./UserMenu";
import {
  Input,
  Label,
  Menu,
  Grid,
  Button,
  Segment,
  Image,
  GridColumn,
  GridRow,
  Header,
  Center,
  MenuHeader,
  Form,
  List
} from "semantic-ui-react";
import { Redirect } from "react-router";
import { getFromStorage, setInStorage } from "../../../utils/storage";
import { DateInput } from "semantic-ui-calendar-react";
const genderOptions = [
  { key: "m", text: "Male", value: "male" },
  { key: "f", text: "Female", value: "female" }
];
export default class UserEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataUser: [],
      token: "",
      name: "",
      signUpError: "",
      city: ""
    };
  }
  componentDidMount() {
    const obj = getFromStorage("the_main_app");

    if (obj && obj.token) {
      const { token } = obj;

      fetch("/api/account/userdata?token=" + token)
        .then(res => res.json())
        .then(json => {
          this.setState({
            dataUser: json.data
          });
        });
    }
  }
  render() {
    const { signUpError, firstName, city } = this.state;
    const datad = [
      {
        firstName: "Erickxxx",
        lastName: "Hadianto",
        hp: "081321703764",
        gender: "Male"
      }
    ];
    // console.log("data user :" + this.state.dataUser.email);
    return (
      <div style={{ marginTop: "10.3em" }}>
        {this.state.dataUser.length > 0 ? (
          <Grid columns="equal">
            <Grid.Column />
            <Grid.Column width={10}>
              <Grid>
                <GridColumn width={4}>
                  <UserMenu />
                </GridColumn>
                <GridColumn width={12}>
                  <Segment>
                    <Header as="h3">Edit Profile</Header>
                  </Segment>
                  <Segment>
                    {signUpError ? (
                      <Message
                        error
                        header="Error Create Account"
                        content={signUpError}
                      />
                    ) : null}
                    <Form>
                      {}
                      <Form.Group widths="equal">
                        <Form.Field
                          id="form-input-control-first-name"
                          control={Input}
                          label="First name"
                          placeholder="First name"
                          value={firstName}
                          onChange={this.onTextboxChangeFirstName}
                          error={this.state.errFirstName}
                        />
                        <Form.Field
                          id="form-input-control-last-name"
                          control={Input}
                          label="Last name"
                          placeholder="Last name"
                          onChange={this.onTextboxChangeLastName}
                          error={this.state.errLastName}
                        />
                      </Form.Group>
                      <Form.Group widths="equal">
                        <Form.Field
                          id="form-input-control-email"
                          control={Input}
                          label="Email Address"
                          placeholder="Email Address"
                          onChange={this.onTextboxChangeEmail}
                          error={this.state.errEmail}
                        />
                        <Form.Select
                          name="fieldGender"
                          options={genderOptions}
                          label="Gender"
                          placeholder="Gender"
                          onChange={this.handleChangeGender}
                          error={this.state.errGender}
                          defaultValue={"male"}
                        />
                      </Form.Group>
                      <Form.Group widths="equal">
                        <Form.Field
                          id="form-input-control-pass1"
                          control={Input}
                          type="password"
                          label="Password"
                          placeholder=""
                          onChange={this.onTextboxChangePassword1}
                          error={this.state.errPassword1}
                        />
                        <Form.Field
                          id="form-input-control-pass2"
                          control={Input}
                          type="password"
                          label="Re-enter"
                          placeholder=""
                          value={this.state.password2}
                          onChange={this.onTextboxChangePassword2}
                          error={this.state.errPasswordMatch}
                        />
                      </Form.Group>
                      <Form.Group widths="equal">
                        <Form.Field
                          id="form-input-control-place-birth"
                          control={Input}
                          label="Place of Birth"
                          placeholder="Place Of Birth"
                          onChange={this.onTextboxChangePlaceBirth}
                          error={this.state.errCityBirth}
                        />
                        <DateInput
                          name="dateBirth"
                          placeholder="Date"
                          value={this.state.dateBirth}
                          iconPosition="left"
                          onChange={this.handleChange}
                          label="Date of Birth"
                          closeOnMouseLeave={false}
                          dateFormat={"MM/DD/YYYY"}
                          closable={true}
                          readOnly={true}
                          error={this.state.errDateBirth}
                        />
                      </Form.Group>
                      <Form.Group widths="equal">
                        <Form.Field
                          id="form-input-control-city"
                          control={Input}
                          label="City"
                          placeholder="City"
                          value={city}
                          onChange={this.onTextboxChangeCity}
                          error={this.state.errCity}
                        />
                        <Form.Field
                          id="form-input-control-phone"
                          control={Input}
                          label="Phone/Handphone"
                          placeholder="Phone Number"
                          onChange={this.onTextboxChangePhone}
                          error={this.state.errPhone}
                        />
                      </Form.Group>
                      <Form.Field
                        id="form-button-control-public"
                        control={Button}
                        content="Submit"
                        onClick={this.onSubmit}
                      />
                    </Form>
                  </Segment>
                </GridColumn>
              </Grid>
            </Grid.Column>
            <Grid.Column />
          </Grid>
        ) : null}
      </div>
    );
  }
}
