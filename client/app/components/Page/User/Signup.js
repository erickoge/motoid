import React, { Component } from "react";

import {
  Input,
  Label,
  Menu,
  Grid,
  Segment,
  Image,
  GridColumn,
  GridRow,
  Header,
  Form,
  Select,
  Button,
  TextArea,
  Message
} from "semantic-ui-react";
import { DateInput } from "semantic-ui-calendar-react";
import { Redirect } from "react-router";
const genderOptions = [
  { key: "m", text: "Male", value: "male" },
  { key: "f", text: "Female", value: "female" }
];
export default class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      gender: "",
      city: "",
      dateBirth: "",
      cityBirth: "",
      phone: "",
      email: "",
      password: "",
      signUpError: "",
      password1: "",
      password2: "",
      //error
      errFirstName: false,
      errLastName: false,
      errGender: false,
      errCity: false,
      errDateBirth: false,
      errCityBirth: false,
      errPhone: false,
      errEmail: false,
      errPassword1: false,
      errPassword2: false,
      errPasswordMatch: false,
      errForm: false,
      errUserCreate: false
    };
    this.onTextboxChangeFirstName = this.onTextboxChangeFirstName.bind(this);
    this.onTextboxChangeLastName = this.onTextboxChangeLastName.bind(this);
    this.onTextboxChangeEmail = this.onTextboxChangeEmail.bind(this);
    this.onTextboxChangeGender = this.onTextboxChangeGender.bind(this);
    this.onTextboxChangeCity = this.onTextboxChangeCity.bind(this);
    this.onTextboxChangeDateBirth = this.onTextboxChangeDateBirth.bind(this);
    this.onTextboxChangePassword1 = this.onTextboxChangePassword1.bind(this);
    this.onTextboxChangePassword2 = this.onTextboxChangePassword2.bind(this);
    this.onTextboxChangePhone = this.onTextboxChangePhone.bind(this);
    this.onTextboxChangePlaceBirth = this.onTextboxChangePlaceBirth.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  handleChange = (event, { name, value }) => {
    if (this.state.hasOwnProperty(name)) {
      //console.log([name] + ":" + value);
      this.setState({ [name]: value });
    }
  };
  handleChangeGender = (e, { value }) => {
    //alert(value);
    this.setState({ gender: value });
  };
  onTextboxChangePlaceBirth(event) {
    this.setState({
      cityBirth: event.target.value
    });
  }
  onTextboxChangeFirstName(event) {
    this.setState({
      firstName: event.target.value
    });
  }
  onTextboxChangeLastName(event) {
    this.setState({
      lastName: event.target.value
    });
  }
  onTextboxChangeDateBirth(event) {
    let dateBefore = event.target.value;
    let ISODateMonth = dateBefore.getMonth();
    let ISODateDay = dateBefore.getDate();
    let ISODateYear = dateBefore.getFullYear();
    let ISODateALL = ISODateMonth + "/" + ISODateDay + "/" + ISODateYear;
    console.log("ISODATEALL :" + ISODateALL);
    this.setState({
      // dateBirth: event.target.value
      dateBirth: ISODateALL
    });
  }
  onTextboxChangeEmail(event) {
    this.setState({
      email: event.target.value
    });
  }
  onTextboxChangeGender(event) {
    console.log("Gender: " + event.target.value);
    this.setState({
      gender: event.target.value
    });
  }
  onTextboxChangePassword1(event) {
    this.setState({
      password1: event.target.value
    });
  }
  onTextboxChangePassword2(event) {
    this.setState({
      password2: event.target.value
    });
  }
  onTextboxChangePhone(event) {
    this.setState({
      phone: event.target.value
    });
  }
  onTextboxChangeCity(event) {
    this.setState({
      city: event.target.value
    });
  }

  onSubmit(e) {
    const {
      firstName,
      lastName,
      gender,
      city,
      dateBirth,
      cityBirth,
      phone,
      email,
      password1,
      password2
    } = this.state;
    e.preventDefault();
    let error = false;

    if (this.state.firstName === "") {
      this.setState({ errFirstName: true });
      error = true;
    } else {
      this.setState({ errFirstName: false });
    }
    if (this.state.lastName === "") {
      this.setState({ errLastName: true });
      error = true;
    } else {
      this.setState({ errLastName: false });
    }
    if (this.state.email === "") {
      this.setState({ errEmail: true });
      error = true;
    } else {
      this.setState({ errEmail: false });
    }

    //errPassword1
    if (this.state.password1 === "") {
      this.setState({ errPassword1: true });
      error = true;
    } else {
      this.setState({ errPassword1: false });
    }
    // errPasswordMatch
    if (this.state.password1 != this.state.password2) {
      this.setState({ errPasswordMatch: true });

      this.setState({ password2: "" });
      error = true;
    } else {
      this.setState({ errPasswordMatch: false });
    }
    if (error) {
      this.setState({ errForm: true });
      return;
    }
    fetch("api/account/register", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        gender: gender,
        city: city,
        dateBirth: dateBirth,
        cityBirth: cityBirth,
        phone: phone,
        email: email,
        password1: password1,
        password2: password2
      })
    })
      .then(res => res.json())
      .then(json => {
        console.log("json", json);
        if (json.success) {
          this.setState({
            signUpError: json.message
            // isLoading: false,
            // signUpEmail: "",
            // signUpPassword: ""
          });
          this.props.history.push("/");
        } else {
          this.setState({
            signUpError: json.message
            //isLoading: false
          });
        }
      });
  }
  render() {
    const {
      firstName,
      lastName,
      gender,
      city,
      dateBirth,
      cityBirth,
      phone,
      signUpError
    } = this.state;
    return (
      <div style={{ marginTop: "10.3em" }}>
        <Grid columns="equal">
          <Grid.Column />
          <Grid.Column width={10}>
            <div>
              <Segment>
                <Header as="h3">Member Registration/Signup</Header>
              </Segment>
              <Segment>
                {signUpError ? (
                  <Message
                    error
                    header="Error Create Account"
                    content={signUpError}
                  />
                ) : null}
                <Form>
                  {}
                  <Form.Group widths="equal">
                    <Form.Field
                      id="form-input-control-first-name"
                      control={Input}
                      label="First name"
                      placeholder="First name"
                      value={firstName}
                      onChange={this.onTextboxChangeFirstName}
                      error={this.state.errFirstName}
                    />
                    <Form.Field
                      id="form-input-control-last-name"
                      control={Input}
                      label="Last name"
                      placeholder="Last name"
                      onChange={this.onTextboxChangeLastName}
                      error={this.state.errLastName}
                    />
                  </Form.Group>
                  <Form.Group widths="equal">
                    <Form.Field
                      id="form-input-control-email"
                      control={Input}
                      label="Email Address"
                      placeholder="Email Address"
                      onChange={this.onTextboxChangeEmail}
                      error={this.state.errEmail}
                    />
                    <Form.Select
                      name="fieldGender"
                      options={genderOptions}
                      label="Gender"
                      placeholder="Gender"
                      onChange={this.handleChangeGender}
                      error={this.state.errGender}
                      defaultValue={"male"}
                    />
                  </Form.Group>
                  <Form.Group widths="equal">
                    <Form.Field
                      id="form-input-control-pass1"
                      control={Input}
                      type="password"
                      label="Password"
                      placeholder=""
                      onChange={this.onTextboxChangePassword1}
                      error={this.state.errPassword1}
                    />
                    <Form.Field
                      id="form-input-control-pass2"
                      control={Input}
                      type="password"
                      label="Re-enter"
                      placeholder=""
                      value={this.state.password2}
                      onChange={this.onTextboxChangePassword2}
                      error={this.state.errPasswordMatch}
                    />
                  </Form.Group>
                  <Form.Group widths="equal">
                    <Form.Field
                      id="form-input-control-place-birth"
                      control={Input}
                      label="Place of Birth"
                      placeholder="Place Of Birth"
                      onChange={this.onTextboxChangePlaceBirth}
                      error={this.state.errCityBirth}
                    />
                    <DateInput
                      name="dateBirth"
                      placeholder="Date"
                      value={this.state.dateBirth}
                      iconPosition="left"
                      onChange={this.handleChange}
                      label="Date of Birth"
                      closeOnMouseLeave={false}
                      dateFormat={"MM/DD/YYYY"}
                      closable={true}
                      readOnly={true}
                      error={this.state.errDateBirth}
                    />
                  </Form.Group>
                  <Form.Group widths="equal">
                    <Form.Field
                      id="form-input-control-city"
                      control={Input}
                      label="City"
                      placeholder="City"
                      value={city}
                      onChange={this.onTextboxChangeCity}
                      error={this.state.errCity}
                    />
                    <Form.Field
                      id="form-input-control-phone"
                      control={Input}
                      label="Phone/Handphone"
                      placeholder="Phone Number"
                      onChange={this.onTextboxChangePhone}
                      error={this.state.errPhone}
                    />
                  </Form.Group>
                  <Form.Field
                    id="form-button-control-public"
                    control={Button}
                    content="Submit"
                    onClick={this.onSubmit}
                  />
                </Form>
              </Segment>
            </div>
          </Grid.Column>
          <Grid.Column />
        </Grid>
      </div>
    );
  }
}
