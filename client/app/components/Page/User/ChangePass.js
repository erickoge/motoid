import React, { Component } from "react";
import Users from "../Users";
import UserMenu from "./UserMenu";
import {
  Input,
  Label,
  Menu,
  Grid,
  Segment,
  Image,
  GridColumn,
  GridRow,
  Header,
  Center,
  MenuHeader,
  List,
  Item,
  Icon,
  Button,
  Form
} from "semantic-ui-react";
import { Redirect } from "react-router";
import { getFromStorage, setInStorage } from "../../../utils/storage";

const paragraph = (
  <Image src="https://react.semantic-ui.com/images/wireframe/short-paragraph.png" />
);
export default class ChangePass extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataUser: [],
      token: ""
    };
  }
  componentDidMount() {
    const obj = getFromStorage("the_main_app");

    if (obj && obj.token) {
      const { token } = obj;

      fetch("/api/account/userdata?token=" + token)
        .then(res => res.json())
        .then(json => {
          this.setState({
            dataUser: json.data
          });
        });
    }
  }
  render() {
    // const { dataUser } = this.state;

    console.log("data user :" + this.state.dataUser.email);
    return (
      <div style={{ marginTop: "10.3em" }}>
        {this.state.dataUser.length > 0 ? (
          <Grid columns="equal">
            <Grid.Column />
            <Grid.Column width={10}>
              <Grid>
                <GridColumn width={4}>
                  <UserMenu />
                </GridColumn>
                <GridColumn width={12}>
                  <Segment>
                    <Header as="h3">Change Password</Header>
                  </Segment>
                  <Grid columns="equal">
                    <GridColumn>
                      <Form>
                        {}
                        <Form.Group widths="equal">
                          <Form.Field
                            id="form-input-control-pass1"
                            control={Input}
                            type="password"
                            label="Password"
                            placeholder=""
                            onChange={this.onTextboxChangePassword1}
                            error={this.state.errPassword1}
                          />
                          <Form.Field
                            id="form-input-control-pass2"
                            control={Input}
                            type="password"
                            label="Re-enter"
                            placeholder=""
                            value={this.state.password2}
                            onChange={this.onTextboxChangePassword2}
                            error={this.state.errPasswordMatch}
                          />
                        </Form.Group>
                        <Form.Field
                          id="form-button-control-public"
                          control={Button}
                          content="Submit"
                          onClick={this.onSubmit}
                        />
                      </Form>
                    </GridColumn>
                  </Grid>
                </GridColumn>
              </Grid>
            </Grid.Column>
            <Grid.Column />
          </Grid>
        ) : null}
      </div>
    );
  }
}
