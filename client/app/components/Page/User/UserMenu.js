import React, { Component } from "react";

import {
  Input,
  Label,
  Menu,
  Grid,
  Segment,
  Image,
  GridColumn,
  GridRow,
  Header,
  Center,
  MenuHeader,
  List
} from "semantic-ui-react";
import { Redirect } from "react-router";
import { Link, withRouter } from "react-router-dom";
class UserMenu extends Component {
  state = { activeItem: "Profile", navigate: false };

  handleItemClick = (e, { name }) => {
    this.setState({ activeItem: name, navigate: true });
    if (name === "Profile") {
      this.props.history.push("/user/profile/");
    } else if (name === "Edit") {
      this.props.history.push("/user/edit/");
    } else if (name === "WatchList") {
      this.props.history.push("/user/watchlist/");
    } else if (name === "ItemSale") {
      this.props.history.push("/user/itemsale/");
    } else if (name === "ChangePass") {
      this.props.history.push("/user/changepass/");
    }

    // return <Redirect to="/product" push={true} />;
  };
  render() {
    const { activeItem, navigate } = this.state;
    if (navigate) {
      if (activeItem === "Profile") {
      } else {
        //  return <Link to="/profile" />;
      }
      if (activeItem === "Edit") {
        //this.props.history.push("/categories/");
        //return <Redirect to="/" push={true} />;
      }
      //console.log("-" + activeItem + "-");
    }
    return (
      <Menu size="large" vertical>
        <MenuHeader>
          <Header as="h2" icon textAlign="center">
            <i className="huge circular user link icon center" />
          </Header>
          <Header as="h3" icon textAlign="center">
            Erick Hadianto
          </Header>
          <Header as="h5" icon textAlign="center">
            Join At July 2018{" "}
          </Header>
        </MenuHeader>
        <Menu.Item
          name="Profile"
          active={activeItem === "Profile"}
          onClick={this.handleItemClick}
        >
          Profile
        </Menu.Item>

        <Menu.Item
          name="Edit"
          active={activeItem === "Edit"}
          onClick={this.handleItemClick}
        >
          Edit Profile
        </Menu.Item>

        <Menu.Item
          name="WatchList"
          active={activeItem === "WatchList"}
          onClick={this.handleItemClick}
        >
          <Label color="teal">1</Label>
          Watch List
        </Menu.Item>

        <Menu.Item
          name="ItemSale"
          active={activeItem === "ItemSale"}
          onClick={this.handleItemClick}
        >
          <Label color="red">21</Label>
          Items For Sell
        </Menu.Item>
        <Menu.Item
          name="ChangePass"
          active={activeItem === "ChangePass"}
          onClick={this.handleItemClick}
        >
          Change Password
        </Menu.Item>
      </Menu>
    );
  }
}
export default withRouter(UserMenu);
