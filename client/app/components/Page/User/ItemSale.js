import React, { Component } from "react";
import Users from "../Users";
import UserMenu from "./UserMenu";
import MyProduct from "../Product/MyProduct";
import {
  Input,
  Label,
  Menu,
  Grid,
  Segment,
  Image,
  GridColumn,
  GridRow,
  Header,
  Center,
  MenuHeader,
  List,
  Item,
  Icon,
  Button
} from "semantic-ui-react";
import { Redirect } from "react-router";
import { getFromStorage, setInStorage } from "../../../utils/storage";
var util = require("util");

const paragraph = (
  <Image src="https://react.semantic-ui.com/images/wireframe/short-paragraph.png" />
);
export default class ItemSale extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataUser: [],
      token: "",
      userId: ""
    };
  }
  componentDidMount() {
    const obj = getFromStorage("the_main_app");

    if (obj && obj.token) {
      const { token } = obj;

      fetch("/api/account/userdata?token=" + token)
        .then(res => res.json())
        .then(json => {
          this.setState({
            dataUser: json.data
          });
          // console.log("dataUser=>" + util.inspect(json.data[0]._id));
        });
    }
  }
  render() {
    const { dataUser } = this.state;
    const datad = [
      {
        firstName: "Erickxxx",
        lastName: "Hadianto",
        hp: "081321703764",
        gender: "Male"
      }
    ];
    // console.log("data user :" + this.state.dataUser.email);
    return (
      <div style={{ marginTop: "10.3em" }}>
        {this.state.dataUser.length > 0 ? (
          <Grid columns="equal">
            <Grid.Column />
            <Grid.Column width={10}>
              <Grid>
                <GridColumn width={4}>
                  <UserMenu />
                </GridColumn>
                <GridColumn width={12}>
                  <Segment>
                    <Header as="h3">Item For Sale</Header>
                  </Segment>
                  <Grid columns="equal">
                    <GridColumn>
                      <MyProduct userId={this.state.dataUser[0]._id} />
                    </GridColumn>
                  </Grid>
                </GridColumn>
              </Grid>
            </Grid.Column>
            <Grid.Column />
          </Grid>
        ) : null}
      </div>
    );
  }
}
