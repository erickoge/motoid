import React, { Component } from "react";
import Users from "../Users";
import UserMenu from "./UserMenu";
import {
  Input,
  Label,
  Menu,
  Grid,
  Segment,
  Image,
  GridColumn,
  GridRow,
  Header,
  Center,
  MenuHeader,
  List
} from "semantic-ui-react";
import { Redirect } from "react-router";
import { getFromStorage, setInStorage } from "../../../utils/storage";
export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataUser: [],
      token: ""
    };
  }
  componentDidMount() {
    const obj = getFromStorage("the_main_app");

    if (obj && obj.token) {
      const { token } = obj;

      fetch("/api/account/userdata?token=" + token)
        .then(res => res.json())
        .then(json => {
          this.setState({
            dataUser: json.data
          });
        });
    }
  }
  render() {
    // const { dataUser } = this.state;
    const datad = [
      {
        firstName: "Erickxxx",
        lastName: "Hadianto",
        hp: "081321703764",
        gender: "Male"
      }
    ];
    console.log("data user :" + this.state.dataUser.email);
    return (
      <div style={{ marginTop: "10.3em" }}>
        {this.state.dataUser.length > 0 ? (
          <Grid columns="equal">
            <Grid.Column />
            <Grid.Column width={10}>
              <Grid>
                <GridColumn width={4}>
                  <UserMenu />
                </GridColumn>
                <GridColumn width={12}>
                  <Segment>
                    <Header as="h3">My Profile</Header>
                  </Segment>
                  <Grid columns="equal">
                    <GridColumn>
                      <List divided relaxed>
                        <List.Item>
                          <List.Icon
                            name="user"
                            size="large"
                            verticalAlign="middle"
                          />
                          <List.Content>
                            <List.Header as="a">First Name</List.Header>
                            <List.Description as="a">
                              {this.state.dataUser[0].firstName}
                            </List.Description>
                          </List.Content>
                        </List.Item>
                        <List.Item>
                          <List.Icon
                            name="github"
                            size="large"
                            verticalAlign="middle"
                          />
                          <List.Content>
                            <List.Header as="a">Gender </List.Header>
                            <List.Description as="a">
                              {this.state.dataUser[0].gender}{" "}
                            </List.Description>
                          </List.Content>
                        </List.Item>
                        <List.Item>
                          <List.Icon
                            name="github"
                            size="large"
                            verticalAlign="middle"
                          />
                          <List.Content>
                            <List.Header as="a">Place Of Birth </List.Header>
                            <List.Description as="a">
                              {this.state.dataUser[0].cityBirth}{" "}
                            </List.Description>
                          </List.Content>
                        </List.Item>
                        <List.Item>
                          <List.Icon
                            name="github"
                            size="large"
                            verticalAlign="middle"
                          />
                          <List.Content>
                            <List.Header as="a">Email Address </List.Header>
                            <List.Description as="a">
                              {this.state.dataUser[0].email}{" "}
                            </List.Description>
                          </List.Content>
                        </List.Item>
                      </List>
                    </GridColumn>
                    <GridColumn>
                      <List divided relaxed>
                        <List.Item>
                          <List.Icon
                            name="github"
                            size="large"
                            verticalAlign="middle"
                          />
                          <List.Content>
                            <List.Header as="a">Last Name </List.Header>
                            <List.Description as="a">
                              {this.state.dataUser[0].lastName}
                            </List.Description>
                          </List.Content>
                        </List.Item>
                        <List.Item>
                          <List.Icon
                            name="github"
                            size="large"
                            verticalAlign="middle"
                          />
                          <List.Content>
                            <List.Header as="a">Role </List.Header>
                            <List.Description as="a">
                              {this.state.dataUser[0].role}{" "}
                            </List.Description>
                          </List.Content>
                        </List.Item>
                        <List.Item>
                          <List.Icon
                            name="github"
                            size="large"
                            verticalAlign="middle"
                          />
                          <List.Content>
                            <List.Header as="a">Date Of Birth </List.Header>
                            <List.Description as="a">
                              {this.state.dataUser[0].birthDate}{" "}
                            </List.Description>
                          </List.Content>
                        </List.Item>
                        <List.Item>
                          <List.Icon
                            name="github"
                            size="large"
                            verticalAlign="middle"
                          />
                          <List.Content>
                            <List.Header as="a">Phone </List.Header>
                            <List.Description as="a">
                              {this.state.dataUser[0].phone}{" "}
                            </List.Description>
                          </List.Content>
                        </List.Item>
                      </List>
                    </GridColumn>
                  </Grid>
                </GridColumn>
              </Grid>
            </Grid.Column>
            <Grid.Column />
          </Grid>
        ) : null}
      </div>
    );
  }
}
