import React, { Component } from "react";
import Users from "../Users";
import UserMenu from "./UserMenu";
import {
  Input,
  Label,
  Menu,
  Grid,
  Segment,
  Image,
  GridColumn,
  GridRow,
  Header,
  Center,
  MenuHeader,
  List,
  Item,
  Icon,
  Button
} from "semantic-ui-react";
import { Redirect } from "react-router";
import { getFromStorage, setInStorage } from "../../../utils/storage";

const paragraph = (
  <Image src="https://react.semantic-ui.com/images/wireframe/short-paragraph.png" />
);
export default class WatchList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataUser: [],
      token: ""
    };
  }
  componentDidMount() {
    const obj = getFromStorage("the_main_app");

    if (obj && obj.token) {
      const { token } = obj;

      fetch("/api/account/userdata?token=" + token)
        .then(res => res.json())
        .then(json => {
          this.setState({
            dataUser: json.data
          });
        });
    }
  }
  render() {
    // const { dataUser } = this.state;
    const datad = [
      {
        firstName: "Erickxxx",
        lastName: "Hadianto",
        hp: "081321703764",
        gender: "Male"
      }
    ];
    console.log("data user :" + this.state.dataUser.email);
    return (
      <div style={{ marginTop: "10.3em" }}>
        {this.state.dataUser.length > 0 ? (
          <Grid columns="equal">
            <Grid.Column />
            <Grid.Column width={10}>
              <Grid>
                <GridColumn width={4}>
                  <UserMenu />
                </GridColumn>
                <GridColumn width={12}>
                  <Segment>
                    <Header as="h3">Watch LIst</Header>
                  </Segment>
                  <Grid columns="equal">
                    <GridColumn>
                      <Item.Group divided unstackable>
                        <Item>
                          <Item.Image
                            size="small"
                            src="/assets/img/250x250.jpg"
                          />

                          <Item.Content verticalAlign="top">
                            <Item.Header>
                              Knalpot Harley Fatboy Standar Original dan Mulus
                              Full System{" "}
                            </Item.Header>
                            <Item.Meta>
                              <Label size="huge">Rp.225.000.000</Label>
                              <Icon name="map marker alternate" />
                              <span className="cinema">
                                Kota Bandung | 11 Watchers
                              </span>
                            </Item.Meta>

                            <Item.Description>{paragraph}</Item.Description>
                            <Item.Extra>
                              <Label>American Motorcycle</Label>
                              <Label>Harley Davidson</Label>
                              <Button primary floated="right">
                                More Detail
                              </Button>
                            </Item.Extra>
                          </Item.Content>
                        </Item>
                      </Item.Group>
                    </GridColumn>
                  </Grid>
                </GridColumn>
              </Grid>
            </Grid.Column>
            <Grid.Column />
          </Grid>
        ) : null}
      </div>
    );
  }
}
