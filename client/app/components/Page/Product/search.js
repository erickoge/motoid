import React, { Component } from "react";
import {
  Divider,
  Image,
  Grid,
  GridColumn,
  Segment,
  Item,
  Button,
  Label,
  Icon,
  Form,
  Dropdown
} from "semantic-ui-react";
import ProductSearch from "./ProductSearch";
var util = require("util");

class SearchResult extends Component {
  constructor(match) {
    super();
    this.match = match;
    this.state = { searchKey: "" };
    this.setMyState = this.setMyState.bind(this);
  }
  setMyState = value => {
    alert("initial state :" + value);
    this.setState({ searchKey: value });
  };
  componentWillReceiveProps(nextProps) {
    alert(this.props.location.state.searchKey);
    this.setState({ searchKey: this.props.location.state.searchKey });
  }
  componentDidMount() {
    /*if (this.props.location.state) {
      alert("dari search field");
      this.setMyState(this.props.location.state.searchKey);
      //  this.setState({ searchKey: this.props.location.state.searchKey });
      console.log(
        "searchKey from search field=>" + util.inspect(this.state.searchKey)
      ); // this.props.location.state.searchKey);
    }
    if (this.match.match.params.searchKey) {
      alert("dari search menu");
      this.setMyState(this.match.match.params.searchKey);
      //this.setState({ searchKey: this.match.match.params.searchKey });
      console.log(
        "searchKey from menu=>" +
          util.inspect(this.match.match.params.searchKey)
      );
    }*/
    //const { searchKey } = this.props.match.params;
    //const { searchKey } = this.match.match.params.searchKey;
    console.log(
      "searchKey=>" + util.inspect(this.props.location.state.searchKey)
    ); // this.props.location.state.searchKey);
  }
  render() {
    const options = [
      { key: 1, text: "Choice 1", value: 1 },
      { key: 2, text: "Choice 2", value: 2 },
      { key: 3, text: "Choice 3", value: 3 }
    ];
    const brandOptions = [
      { key: "af", value: "af", text: "Harley Davidson" },
      { key: "af2", value: "af2", text: "Indian" },
      { key: "af3", value: "af3", text: "Polaris" },
      { key: "af4", value: "4", text: "Victory" }
    ];
    const conditionOptions = [
      { key: "af", value: "af", text: "New" },
      { key: "af2", value: "af2", text: "Used" }
    ];
    const capacityOptions = [
      { key: "af", value: "af", text: "150cc" },
      { key: "af2", value: "af", text: "250cc" },
      { key: "af3", value: "af", text: "350cc" },
      { key: "af4", value: "af2", text: ">500cc" }
    ];
    const cityOptions = [
      { key: "af", value: "af", text: "Bandung" },
      { key: "af1", value: "af", text: "Jakarta" },
      { key: "af2", value: "af", text: "Surabaya" },
      { key: "af3", value: "af2", text: "Bogor" }
    ];
    const paragraph = (
      <Image src="https://react.semantic-ui.com/images/wireframe/short-paragraph.png" />
    );
    return (
      <div style={{ marginTop: "10.3em" }}>
        <Grid columns="equal">
          <Grid.Column />
          <Grid.Column width={10}>
            <Grid>
              <GridColumn width={5}>
                <Segment color="blue">
                  <Form>
                    <Form.Group>
                      <Dropdown
                        clearable
                        fluid
                        multiple
                        search
                        selection
                        options={brandOptions}
                        placeholder="Select Brand"
                      />
                    </Form.Group>
                    <Form.Group>
                      <Dropdown
                        clearable
                        fluid
                        multiple
                        search
                        selection
                        options={brandOptions}
                        placeholder="Select Type"
                      />
                    </Form.Group>
                    <Form.Group>
                      <Form.Input
                        label="Year Start"
                        placeholder="Year Start"
                        width={8}
                      />
                      <Form.Input
                        label="Year End"
                        placeholder="Year End"
                        width={8}
                      />
                    </Form.Group>
                    <Form.Group>
                      <Dropdown
                        clearable
                        fluid
                        multiple
                        search
                        selection
                        options={capacityOptions}
                        placeholder="Engine Capacity"
                      />
                    </Form.Group>
                    <Form.Group>
                      <Dropdown
                        clearable
                        fluid
                        multiple
                        search
                        selection
                        options={conditionOptions}
                        placeholder="Conditions"
                      />
                    </Form.Group>
                    <Form.Group>
                      <Dropdown
                        clearable
                        fluid
                        multiple
                        search
                        selection
                        options={cityOptions}
                        placeholder="City"
                      />
                    </Form.Group>
                    <Form.Group>
                      <Form.Input label="Min Price" placeholder="" width={8} />
                      <Form.Input label="Max Price" placeholder="" width={8} />
                    </Form.Group>
                  </Form>
                </Segment>
              </GridColumn>
              <GridColumn width={11}>
                <ProductSearch searchTxt={this.state.searchKey} />
              </GridColumn>
            </Grid>
          </Grid.Column>
          <Grid.Column />
        </Grid>
      </div>
    );
  }
}
export default SearchResult;
/*
{this.props.location.state.searchKey}
<Item.Group divided unstackable>
                  <Item>
                    <Item.Image size="small" src="/assets/img/250x250.jpg" />

                    <Item.Content verticalAlign="top">
                      <Item.Header>
                        Knalpot Harley Fatboy Standar Original dan Mulus Full
                        System{" "}
                      </Item.Header>
                      <Item.Meta>
                        <Label size="huge">Rp.225.000.000</Label>
                        <Icon name="map marker alternate" />
                        <span className="cinema">
                          Kota Bandung | 11 Watchers
                        </span>
                      </Item.Meta>

                      <Item.Description>{paragraph}</Item.Description>
                      <Item.Extra>
                        <Label>American Motorcycle</Label>
                        <Label>Harley Davidson</Label>
                        <Button primary floated="right">
                          More Detail
                        </Button>
                      </Item.Extra>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Image size="small" src="/assets/img/250x250.jpg" />

                    <Item.Content verticalAlign="top">
                      <Item.Header>
                        Knalpot Harley Fatboy Standar Original dan Mulus Full
                        System{" "}
                      </Item.Header>
                      <Item.Meta>
                        <Label size="huge">Rp.225.000.000</Label>
                        <Icon name="map marker alternate" />
                        <span className="cinema">
                          Kota Bandung | 11 Watchers
                        </span>
                      </Item.Meta>

                      <Item.Description>{paragraph}</Item.Description>
                      <Item.Extra>
                        <Label>American Motorcycle</Label>
                        <Label>Harley Davidson</Label>
                        <Button primary floated="right">
                          More Detail
                        </Button>
                      </Item.Extra>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Image size="small" src="/assets/img/250x250.jpg" />

                    <Item.Content verticalAlign="top">
                      <Item.Header>
                        Knalpot Harley Fatboy Standar Original dan Mulus Full
                        System{" "}
                      </Item.Header>
                      <Item.Meta>
                        <Label size="huge">Rp.225.000.000</Label>
                        <Icon name="map marker alternate" />
                        <span className="cinema">
                          Kota Bandung | 11 Watchers
                        </span>
                      </Item.Meta>

                      <Item.Description>{paragraph}</Item.Description>
                      <Item.Extra>
                        <Label>American Motorcycle</Label>
                        <Label>Harley Davidson</Label>
                        <Button primary floated="right">
                          More Detail
                        </Button>
                      </Item.Extra>
                    </Item.Content>
                  </Item>
                </Item.Group>
*/
