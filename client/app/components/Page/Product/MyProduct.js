import React, { Component } from "react";

import {
  Input,
  Label,
  Menu,
  Grid,
  Segment,
  Image,
  GridColumn,
  GridRow,
  Header,
  Center,
  MenuHeader,
  List,
  Item,
  Icon,
  Button
} from "semantic-ui-react";
import { listByUserId } from "./api-product";
import "./product.css";
import NumberFormat from "react-number-format";

var util = require("util");
export default class MyProduct extends Component {
  state = {
    products: []
  };
  loadProducts = () => {
    listByUserId({
      userId: this.props.userId
    }).then(data => {
      if (data.error) {
        this.setState({ error: data.error });
      } else {
        this.setState({ products: data });
      }
      //console.log("data=>" + util.inspect(this.state.products));
      console.log(
        "kategoris=>" + util.inspect(this.state.products[0].kategoris)
      );
      console.log(
        "kategoris.ancestors=>" +
          util.inspect(this.state.products[0].kategoris.ancestors)
      );
    });
  };
  componentDidMount = () => {
    this.loadProducts();
  };
  render() {
    return (
      <div>
        <Item.Group divided>
          {this.state.products.map((product, i) => {
            return (
              <Item key={i}>
                <Item.Image
                  size="small"
                  src={
                    "/api/product/image/" +
                    product._id +
                    "?" +
                    new Date().getTime()
                  }
                />

                <Item.Content verticalAlign="top">
                  <Item.Header>
                    <b>{product.title}</b>
                  </Item.Header>
                  <Item.Meta>
                    <Label size="huge">
                      <NumberFormat
                        value={product.price}
                        displayType={"text"}
                        thousandSeparator={true}
                        prefix={"Rp."}
                      />
                    </Label>
                    <Icon name="map marker alternate" />
                    <span className="cinema">
                      {product.city.name} | 11 Watchers
                    </span>
                  </Item.Meta>

                  <Item.Description id="p_wrap">
                    {product.desc}
                  </Item.Description>
                  <Item.Extra>
                    {product.kategoris.ancestors.map((kategori, index) => {
                      return <Label key={index}>{kategori}</Label>;
                    })}

                    <Button
                      circular
                      color="google plus"
                      icon="delete"
                      floated="right"
                    />
                    <Button
                      circular
                      color="twitter"
                      icon="edit"
                      floated="right"
                    />
                  </Item.Extra>
                </Item.Content>
              </Item>
            );
          })}
        </Item.Group>
      </div>
    );
  }
}
