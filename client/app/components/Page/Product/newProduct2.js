import React, { Component } from "react";
import {
  Grid,
  GridColumn,
  Segment,
  Form,
  List,
  Input,
  Button,
  Dropdown
} from "semantic-ui-react";

import { create, loadProvince, loadCity } from "./api-product";
import { Redirect } from "react-router-dom";
import Upload from "../../Utils/Upload";
import { create2 } from "./api-product";
var util = require("util");

export default class NewProduct2 extends Component {
  constructor(match) {
    super();
    this.match = match;

    this.state = {
      dataLvl1: []
    };
    this.callFromChild = this.callFromChild.bind(this);
  }
  componentDidMount() {
    this.productData = new FormData();
  }
  callFromChild(fieldName, value) {
    // alert("Fieldname=> " + fieldName + " value=> " + value);
    this.productData.set(fieldName, value);
  }

  handleChange = name => (event, { value }) => {
    var values;
    if (event.key === "Enter") {
      return;
    }
    alert("name =>" + name + " Values=>" + values);
    if (event.target.value) {
      values = name === "image" ? event.target.files[0] : event.target.value;
    } else {
      values = value;
    }
    alert("name =>" + name + " Values=>" + values);
    this.productData.set(name, values);
    //this.setState({ [name]: values });

    if (name === "province") {
      loadCity({ provId: values }).then(data => {
        if (data.error) {
          console.log(data.error);
        } else {
          this.setState({ dataCity: data });
        }
      });
    }
  };
  handleChange = name => (event, { value }) => {
    var values;
    if (event.target.value) {
      values = name === "image" ? event.target.files[0] : event.target.value;
    } else {
      values = value;
    }
    // alert("name =>" + name + " Values=>" + values);
    this.productData.set(name, values);
    this.setState({ [name]: values });
  };
  clickSubmit = () => {
    this.productData.set("_id", this.match.match.params.productId);
    this.setState({ ["_id"]: this.match.match.params.productId });
    console.log("params =>" + util.inspect(this.match.match.params));
    create2(
      {
        token: this.match.match.params.token,
        productId: this.match.match.params.productId
      },

      this.productData
    ).then(data => {
      if (data.error) {
        this.setState({ error: data.error });
      } else {
        this.setState({});
        this.setState({ error: "", redirect: true, productId: data._id });
      }
    });
  };
  render() {
    if (this.state.redirect) {
      return (
        <Redirect to={"/user/itemsale/" + this.match.match.params.token} />
      );
    }
    return (
      <div style={{ marginTop: "10.3em" }}>
        <Grid columns="equal">
          <GridColumn />
          <GridColumn width={10}>
            <Segment>
              <h4>Sell Items on Motostation</h4>
            </Segment>
            <Segment>
              <Form>
                <Form.TextArea
                  label="Description"
                  rows={8}
                  placeholder="Please Fill Your Product Description"
                  onChange={this.handleChange("desc")}
                />
                <label>UPLOAD PHOTOs</label>
                <Segment>
                  <List horizontal>
                    <List.Item>
                      <Upload onChange={this.callFromChild} id="file1" />
                    </List.Item>
                    <List.Item>
                      <Upload onChange={this.callFromChild} id="file2" />
                    </List.Item>
                    <List.Item>
                      <Upload onChange={this.callFromChild} id="file3" />
                    </List.Item>
                    <List.Item>
                      <Upload onChange={this.callFromChild} id="file4" />
                    </List.Item>
                    <List.Item>
                      <Upload onChange={this.callFromChild} id="file5" />
                    </List.Item>
                  </List>
                </Segment>
                <Form.Input label="Tag" onChange={this.handleChange("tag")} />
                <Button type="submit" positive onClick={this.clickSubmit}>
                  Save And Upload
                </Button>
              </Form>
            </Segment>
          </GridColumn>
          <GridColumn />
        </Grid>
      </div>
    );
  }
}
