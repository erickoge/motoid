import queryString from "query-string";
var util = require("util");

const listSuggestMotor = params => {
  console.log("load COntroller")
  return fetch("/api/suggestmotor", {
    method: "GET"
  })
    .then(response => {
      return response.json();
    })
    .catch(err => {
      console.log(err);
    });
};

const listByUserId = params => {
  return fetch("/api/products/by/userId/" + params.userId, {
    method: "GET"
  })
    .then(response => {
      return response.json();
    })
    .catch(err => {
      console.log(err);
    });
};

const loadCity = params => {
  let dataku = "";
  let arr = [];
  return fetch("/api/produk/db/city/" + params.provId, {
    method: "GET"
  })
    .then(response => response.json())
    .then(json => {
      console.log("loadCity=>" + util.inspect(json));
      Object.keys(json).forEach(function(key) {
        dataku = {
          key: key,
          text: json[key].name,
          value: json[key]._id
        };
        arr.push(dataku);
      });

      return arr;
    })
    .catch(err => console.log(err));
};

const loadProvince = () => {
  let arr = [];
  let dataku = "";
  return fetch("/api/produk/db/province", {
    method: "GET"
  })
    .then(response => response.json())
    .then(json => {
      // (jsoconsole.log("loadProvince=>" + util.inspectn));
      Object.keys(json).forEach(function(key) {
        dataku = {
          key: key,
          text: json[key].name,
          value: json[key]._id
        };
        arr.push(dataku);
      });

      return arr;
    })
    .catch(err => console.log(err));
};
const create = (params, product) => {
  return fetch("/api/products/by/" + params.token, {
    method: "POST",
    headers: {
      Accept: "application/json"
    },
    body: product
  })
    .then(response => {
      return response.json();
    })
    .catch(err => console.log(err));
};
const create2 = (params, product) => {
  // alert("param.productId==>" + params.productId);
  return fetch("/api/products/by2/" + params.token + "/" + params.productId, {
    method: "PUT",
    headers: {
      Accept: "application/json"
    },
    body: product
  })
    .then(response => {
      return response.json();
    })
    .catch(err => console.log(err));
};
const listBySearch = params => {
  return fetch("/api/products/by/" + params.searchTxt, {
    method: "GET"
  })
    .then(response => {
      return response.json();
    })
    .catch(err => {
      console.log(err);
    });
};
const list = params => {
  const query = queryString.stringify(params);
  return fetch("/api/products?" + query, {
    method: "GET"
  })
    .then(response => {
      return response.json();
    })
    .catch(err => console.log(err));
};
export {
  listSuggestMotor,
  listByUserId,
  create2,
  create,
  listBySearch,
  list,
  loadCity,
  loadProvince
};
