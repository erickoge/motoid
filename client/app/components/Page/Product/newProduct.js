import React, { Component } from "react";
import {
  Grid,
  GridColumn,
  Segment,
  Form,
  List,
  Input,
  Button,
  Dropdown
} from "semantic-ui-react";
import Upload from "./Upload";
import { create, loadProvince, loadCity } from "./api-product";
import { Redirect } from "react-router-dom";
var util = require("util");

const kondisiOptions = [
  { key: "1", text: "Baru", value: "Baru" },
  { key: "2", text: "Bekas", value: "Bekas" },
  { key: "3", text: "Rekondisi", value: "Rekondisi" }
];
export default class NewProduct extends Component {
  constructor(match) {
    super();
    this.match = match;

    this.state = {
      dataLvl1: [],
      dataLvl11: [
        {
          key: "1",
          text: "American Motorcycle",
          value: "American Motorcycle"
        },
        {
          key: "2",
          text: "European Motorcycle",
          value: "European Motorcycle"
        },
        {
          key: "3",
          text: "Japanese Motorcycle",
          value: "Japanese Motorcycle"
        }
      ],
      dataLvl2: "",
      dataLvl3: "",
      dropdown1: false,
      dropdown2: true,
      dropdown3: true,
      dropdown4: true,
      dropdown5: true,
      valueLvl1: "",
      valueLvl2: "",
      valueLvl3: "",
      valueLvl4: "",
      valueLvl5: "",
      dataProvince: [],
      dataCity: [],
      productId: ""
    };
  }
  componentDidMount() {
    this.productData = new FormData();
    let dataku = "";
    let arr = [];
    var arrSend = ["American Motorcycle", "Motorcycle"];
    fetch("/api/data/kategoriku", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },

      body: JSON.stringify({
        ancestors1: ""
      })
    })
      //fetch("/api/test/kategori?parent=")
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          Object.keys(json.data).forEach(function(key) {
            dataku = {
              key: key,
              text: json.data[key].name,
              value: json.data[key].name
            };
            arr.push(dataku);
          });
          // console.log(json);
          this.setState({
            dataLvl1: arr
          });
        } else {
          console.log(" teu sukses");
        }
      });

    loadProvince().then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({ dataProvince: data });
      }
    });
  }
  onChangeValueLvl1 = (e, { value }) => {
    // alert(value);
    // console.log("token==>" + this.match.match.params.token);
    parent = value;
    let arr = [];

    this.setState({
      valueLvl1: parent
    });
    this.setState({
      dataLvl2: arr
    });
    let dataku = "";
    //fetch("/api/data/kategoris?parent=" + parent)
    fetch("/api/data/kategoriku", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },

      body: JSON.stringify({
        ancestors1: parent
      })
    })
      .then(res => res.json())
      .then(json => {
        console.log(json.data);

        if (json.success) {
          Object.keys(json.data).forEach(function(key) {
            dataku = {
              key: key,
              text: json.data[key].name,
              value: json.data[key].name
            };
            arr.push(dataku);
          });
          this.setState({
            dataLvl2: arr
          });

          this.setState({
            dropdown2: false
          });
          this.setState({
            dropdown3: true
          });
          this.setState({
            dropdown4: true
          });
          this.setState({ dropdown5: true });
          this.setState({ dataLvl3: [] });
          this.setState({ dataLvl5: [] });
          this.setState({ dataLvl4: [] });

          this.setState({
            dropdown3: true
          });
        } else {
          /* this.setState({
            isLoading: false
          });*/
        }
      });
    // this.setState({ gender: value });
  };
  onChangeValueLvl2 = (e, { value }) => {
    //alert(value);
    parent = value;
    let arr2 = [];
    this.setState({
      valueLvl2: value
    });
    this.setState({
      dataLvl3: arr2
    });
    let dataku = "";
    //fetch("/api/data/kategoris?parent=" + parent)
    fetch("/api/data/kategoriku", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },

      body: JSON.stringify({
        ancestors1: this.state.valueLvl1,
        ancestors2: parent
      })
    })
      .then(res => res.json())
      .then(json => {
        console.log(json.data);
        if (json.success) {
          Object.keys(json.data).forEach(function(key) {
            dataku = {
              key: key,
              text: json.data[key].name,
              value: json.data[key].name
            };
            arr2.push(dataku);
          });
          this.setState({
            dataLvl3: arr2
          });
          this.setState({
            dropdown3: false
          });

          this.setState({ dropdown4: true });
          this.setState({ dropdown5: true });

          this.setState({ dataLvl4: [] });
          this.setState({ dataLvl5: [] });
        }
      });
  };
  onChangeValueLvl3 = (e, { value }) => {
    parent = value;
    let arr3 = [];
    this.setState({
      valueLvl3: parent
    });
    this.setState({
      dataLvl4: arr3
    });
    let dataku = "";
    //fetch("/api/data/kategoris?parent=" + parent)
    fetch("/api/data/kategoriku", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },

      body: JSON.stringify({
        ancestors1: this.state.valueLvl1,
        ancestors2: this.state.valueLvl2,
        ancestors3: parent
      })
    })
      .then(res => res.json())
      .then(json => {
        console.log(json.data);
        if (json.success) {
          Object.keys(json.data).forEach(function(key) {
            dataku = {
              key: key,
              text: json.data[key].name,
              value: json.data[key]._id
            };
            arr3.push(dataku);
          });
          this.setState({
            dataLvl4: arr3
          });
          this.setState({
            dropdown4: false
          });

          this.setState({ dropdown5: true });

          this.setState({ dataLvl5: [] });
        }
      });
  };
  onChangeValueLvl4 = (e, { value }) => {
    parent = e.target.textContent;
    //console.log("cari text=>" + e.target.textContent);
    let arr4 = [];
    this.setState({
      valueLvl4: parent
    });
    this.setState({
      dataLvl5: arr4
    });
    let dataku = "";
    if (this.state.valueLvl1 != "American Motorcycle") {
      //fetch("/api/data/kategoris?parent=" + parent)
      fetch("/api/data/kategoriku", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },

        body: JSON.stringify({
          ancestors1: this.state.valueLvl1,
          ancestors2: this.state.valueLvl2,
          ancestors3: this.state.valueLvl3,
          ancestors4: parent
        })
      })
        .then(res => res.json())
        .then(json => {
          console.log(json.data);
          if (json.success) {
            Object.keys(json.data).forEach(function(key) {
              dataku = {
                key: key,
                text: json.data[key].name,
                value: json.data[key]._id
              };
              arr4.push(dataku);
            });
            if (this.state.valueLvl1 === "American Motorcycle") {
              this.setState({
                validCategory: true
              });
              this.productData.set("kategoris", value);
            } else {
              this.setState({
                dataLvl5: arr4
              });
              this.setState({
                dropdown5: false
              });
            }
          }
        });
    } else {
      this.productData.set("kategoris", value);
    }
  };
  onChangeValueLvl5 = (e, { value }) => {
    this.productData.set("kategoris", parent);
  };
  validCategory;
  handleChange = name => (event, { value }) => {
    var values;
    if (event.key === "Enter") {
      return;
    }

    if (event.target.value) {
      values = name === "image" ? event.target.files[0] : event.target.value;
    } else {
      values = value;
    }
    // alert("name =>" + name + " Values=>" + values);
    this.productData.set(name, values);
    this.setState({ [name]: values });

    if (name === "province") {
      loadCity({ provId: values }).then(data => {
        if (data.error) {
          console.log(data.error);
        } else {
          this.setState({ dataCity: data });
        }
      });
    }
  };

  clickSubmit = () => {
    create(
      {
        token: this.match.match.params.token
      },

      this.productData
    ).then(data => {
      if (data.error) {
        this.setState({ error: data.error });
      } else {
        this.setState({});
        this.setState({ error: "", redirect: true, productId: data._id });
      }
    });
  };

  handleKeyDown = e => {
    const re = new RegExp(e.key, "i");
    const match = kondisiOptions.find(o => re.test(o.text));

    if (match) this.setState(() => ({ value: match.value }));
  };
  render() {
    if (this.state.redirect) {
      return (
        <Redirect
          to={
            "/produk/new2/" +
            this.match.match.params.token +
            "/" +
            this.state.productId
          }
        />
      );
    }
    return (
      <div style={{ marginTop: "10.3em" }}>
        <Grid columns="equal">
          <GridColumn />
          <GridColumn width={10}>
            <Segment>
              <h4>Sell Items on Motostation</h4>
            </Segment>
            <Segment>
              <Form>
                <Form.Group>
                  <Form.Select
                    width={5}
                    name="field1"
                    options={this.state.dataLvl1}
                    label="Category"
                    onChange={this.onChangeValueLvl1}
                  />
                  <Form.Select
                    width={5}
                    name="field2"
                    options={this.state.dataLvl2}
                    label="Sub Category1"
                    onChange={this.onChangeValueLvl2}
                    disabled={this.state.dropdown2}
                  />
                  <Form.Select
                    width={5}
                    name="field3"
                    options={this.state.dataLvl3}
                    label="Sub Category2"
                    onChange={this.onChangeValueLvl3}
                    disabled={this.state.dropdown3}
                  />
                </Form.Group>

                <Form.Group>
                  <Form.Select
                    width={5}
                    name="field4"
                    options={this.state.dataLvl4}
                    label="Brand"
                    onChange={this.onChangeValueLvl4}
                    disabled={this.state.dropdown4}
                  />
                  <Form.Select
                    width={5}
                    name="field5"
                    options={this.state.dataLvl5}
                    label="Type"
                    onChange={(e, { value }) => {
                      this.setState({ validCategory: true });
                      this.productData.set("kategoris", value);
                    }}
                    disabled={this.state.dropdown5}
                  />
                </Form.Group>
                <Form.Field
                  id="form-input-control-first-name"
                  name="title"
                  control={Input}
                  label="JUDUL IKLAN"
                  placeholder="Judul Iklan"
                  onChange={this.handleChange("title")}
                />
                <Form.Group widths="equal">
                  <Form.Select
                    name="condition"
                    options={kondisiOptions}
                    label="Condition"
                    onChange={this.handleChange("condition")}
                    onKeyDown={this.handleKeyDown}
                  />
                  <Form.Field
                    id="form-input-control-first-name"
                    name="yearout"
                    control={Input}
                    label="Tahun Pembuatan"
                    placeholder="Tahun Pembuatan"
                    onChange={this.handleChange("yearout")}
                  />
                  <Form.Field
                    id="form-input-control-first-name"
                    name="color"
                    control={Input}
                    label="Color"
                    placeholder="Color"
                    onChange={this.handleChange("color")}
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <Form.Field
                    name="enginecc"
                    control={Input}
                    label="Engine Capacity"
                    placeholder="Kapasitas Mesin"
                    onChange={this.handleChange("enginecc")}
                  />
                  <Form.Field
                    id="kilometer"
                    name="kilometer"
                    control={Input}
                    label="Kilometer"
                    placeholder="Judul Iklan"
                    onChange={this.handleChange("kilometer")}
                  />
                  <Form.Field
                    id="price"
                    name="price"
                    control={Input}
                    label="Price"
                    placeholder="Price"
                    onChange={this.handleChange("price")}
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <Form.Select
                    name="province"
                    options={this.state.dataProvince}
                    label="Province"
                    placeholder="Province"
                    onChange={this.handleChange("province")}
                  />
                  <Form.Select
                    name="city"
                    options={this.state.dataCity}
                    label="City"
                    placeholder="Nama Kota"
                    onChange={this.handleChange("city")}
                  />

                  <Form.Field
                    id="form-input-control-first-name"
                    name="phone"
                    control={Input}
                    label="Phone Number"
                    placeholder="Judul Iklan"
                    onChange={this.handleChange("phone")}
                  />
                </Form.Group>
                <Button
                  type="submit"
                  positive
                  onClick={this.clickSubmit}
                  disabled={
                    !this.state.city ||
                    !this.state.title ||
                    !this.state.condition ||
                    !this.state.yearout ||
                    !this.state.color ||
                    !this.state.enginecc ||
                    !this.state.kilometer ||
                    !this.state.price ||
                    !this.state.province ||
                    !this.state.phone
                  }
                >
                  Next
                </Button>
              </Form>
            </Segment>
          </GridColumn>
          <GridColumn />
        </Grid>
      </div>
    );
  }
}
/*
 */
