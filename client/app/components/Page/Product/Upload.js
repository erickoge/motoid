const React = require("react");
import { Image, Card } from "semantic-ui-react";
import Axios from "axios";
export default class Upload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      file: "/xxx/nofoto.png",
      imgSrc: null,
      nextImg: false,
      baseUrl: "/xxx/"
    };
    this.handleChange = this.handleChange.bind(this);
    this.onChangeFile = this.onChangeFile.bind(this);
    //  this.handleClick = this.handleClick.bind(this);
  }

  handleClick = event => {
    this.refs.fileUploader.click();

    // alert("OK :" + event);
    //this.setState({ nextImg: !this.state.nextImg });
  };
  handleChange(event) {
    this.setState({
      file: URL.createObjectURL(event.target.files[0])
    });
  }

  onChangeFile(event) {
    event.stopPropagation();
    event.preventDefault();
    var file = event.target.files[0];
    const data = new FormData();
    data.append("myImage", file); //this.state.file);
    const config = {
      headers: {
        "Content-Type": "multipart/form-data"
      }
    };
    Axios.post("/api/img/produk", data, config)
      .then(res => {
        console.log("response", res.data);
        if (res.data.success) {
          alert("Filename => " + res.data.filename);
          this.setState({ file: this.state.baseUrl + res.data.filename });
        }
      })
      /*.then(response => {
        alert("The file is successfully uploaded");
      })*/
      .catch(error => {
        alert("Error Happend :" + error);
      });
    //alert("Name of Files:" + file.name);
    /*console.log(file);
    fetch("/api/image/upload", {
      method: "POST",
      body: file
    });
    */
    //this.setState({ file }); /// if you want to upload latter
  }
  render() {
    var imgUrl = this.state.file;
    var divStyle = {
      "background-position": "center",
      backgroundImage: "url(" + imgUrl + ")",
      "background-size": "130px",
      width: "150px",
      height: "150px"
      //"background-size": "cover"
    };

    return (
      <div>
        <img style={divStyle} as="a" onClick={() => this.handleClick()} />
        <input
          type="file"
          id="file"
          ref="fileUploader"
          style={{ display: "none" }}
          onChange={this.onChangeFile.bind(this)}
        />
      </div>
    );
  }
}
/*
<input type="file" id="file" ref="fileUploader" style={{display: "none"}}/>

<input type="file" onChange={this.handleChange} />
 <Image src={this.state.file} size="small" bordered />
 return <Image class="imageupload" src={this.state.file} />;
 <div style={divStyle} onClick={this.handleClick} className="player" />
 <Image src={this.state.file} size="small" bordered />
 */
