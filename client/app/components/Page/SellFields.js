import React, { Component } from "react";
import {
  Grid,
  GridColumn,
  Segment,
  Form,
  Input,
  Radio,
  Header,
  FormField,
  Button
} from "semantic-ui-react";
class SellFields extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div style={{ marginTop: "4.3em" }}>
        <Grid columns="equal">
          <GridColumn />
          <GridColumn width={10}>
            <Segment>
              <Header as="h4">Sell Items on Motostation</Header>
            </Segment>
            <Segment>
              <Form>
                <Form.Field
                  id="form-input-control-first-name"
                  control={Input}
                  label="Title"
                  placeholder="First name"
                  onChange={this.onTextboxChangeFirstName}
                />
                <Form.Group widths="equal">
                  <Form.Field>
                    <label for="form-radio-control-new">Conditions</label>

                    <Radio
                      id="form-radio-control-new"
                      label="New"
                      name="radioGroup"
                      onChange={this.handleChange}
                    />
                  </Form.Field>
                  <Form.Field>
                    <label for="form-radio-control-used">.</label>
                    <Radio
                      id="form-radio-control-used"
                      label="Used"
                      name="radioGroup"
                      onChange={this.handleChange}
                    />
                  </Form.Field>
                  <Form.Field>
                    <label for="form-radio-control-ref">.</label>
                    <Radio
                      id="form-radio-control-ref"
                      label="Refurbish"
                      name="radioGroup"
                      onChange={this.handleChange}
                    />
                  </Form.Field>
                </Form.Group>
                <Form.Field
                  id="form-input-control-first-name"
                  control={Input}
                  label="Engine Capacity"
                  placeholder="cc"
                  onChange={this.onTextboxChangeFirstName}
                />
                <Form.Field
                  id="form-input-control-first-name"
                  control={Input}
                  label="Kilometer"
                  placeholder="Km/Mill"
                  onChange={this.onTextboxChangeFirstName}
                />
                <Form.Field
                  id="form-input-control-first-name"
                  control={Input}
                  label="Color"
                  placeholder="Color"
                  onChange={this.onTextboxChangeFirstName}
                />
                <Form.Field
                  id="form-input-control-first-name"
                  control={Input}
                  label="Province"
                  placeholder="Province"
                  onChange={this.onTextboxChangeFirstName}
                />
                <Form.Field
                  id="form-input-control-first-name"
                  control={Input}
                  label="City"
                  placeholder="City"
                  onChange={this.onTextboxChangeFirstName}
                />
                <Form.Field
                  id="form-input-control-first-name"
                  control={Input}
                  label="Phone Number"
                  placeholder="Phone Number"
                  onChange={this.onTextboxChangeFirstName}
                />
                <Form.Field
                  id="form-button-control-public"
                  control={Button}
                  content="Next"
                  onClick={this.onSubmit}
                />
              </Form>
            </Segment>
          </GridColumn>
          <GridColumn />
        </Grid>
      </div>
    );
  }
}
export default SellFields;
