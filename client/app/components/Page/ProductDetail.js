import React, { Component } from "react";
import {
  Grid,
  Header,
  Label,
  Icon,
  Container,
  Item,
  Image,
  Segment,
  Table,
  TableBody,
  TableRow,
  TableCell,
  Button
} from "semantic-ui-react";
import { Carousel } from "react-responsive-carousel";

class ProductDetail extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const paragraph = "test test";
    return (
      <div style={{ marginTop: "10.3em" }}>
        <Grid columns="equal">
          <Grid.Column />
          <Grid.Column width={12}>
            <Grid>
              <Grid.Column width={6}>
                <div styles="height:800px">
                  <Carousel
                    showThumbs={true}
                    autoPlay={true}
                    showArrows={false}
                    showStatus={false}
                    showIndicators={false}
                    infiniteLoop={true}
                    dynamicHeight={true}
                  >
                    <div>
                      <img src="assets/img/11.jpg" />
                    </div>
                    <div>
                      <img src="assets/img/21.jpg" />
                    </div>
                    <div>
                      <img src="assets/img/31.jpg" />
                    </div>
                  </Carousel>
                </div>
                <div>
                  <Segment>
                    <Header>Description </Header>
                  </Segment>
                  <Segment>
                    <p>
                      Text Description Text Description Text Description Text
                      Description Text Description Text Description{" "}
                    </p>
                    <p>
                      Text Description Text Description Text Description Text
                      Description Text Description Text Description{" "}
                    </p>
                    <p>
                      Text Description Text Description Text Description Text
                      Description Text Description Text Description{" "}
                    </p>
                  </Segment>
                </div>
              </Grid.Column>
              <Grid.Column width={10}>
                <Item.Group>
                  <Item>
                    <Item.Content>
                      <Item.Header as="h3">
                        Knalpot Harley Fatboy Standard,Original dan Mulus Full
                        System
                      </Item.Header>
                      <Item.Meta>
                        <span className="ui.meta">
                          part.american | 11 watcher
                        </span>
                      </Item.Meta>

                      <Label size="huge">Rp.110.000.000</Label>

                      <Grid columns={2}>
                        <Grid.Column>
                          <Table>
                            <TableBody>
                              <TableRow>
                                <TableCell>Brand</TableCell>
                                <TableCell>:</TableCell>
                                <TableCell>Harley Davidson</TableCell>
                              </TableRow>
                              <TableRow>
                                <TableCell>Mc Type</TableCell>
                                <TableCell>:</TableCell>
                                <TableCell>Harley Davidson</TableCell>
                              </TableRow>
                              <TableRow>
                                <TableCell>Color</TableCell>
                                <TableCell>:</TableCell>
                                <TableCell>Harley Davidson</TableCell>
                              </TableRow>
                              <TableRow>
                                <TableCell>Engine Capacity</TableCell>
                                <TableCell>:</TableCell>
                                <TableCell>Harley Davidson</TableCell>
                              </TableRow>
                              <TableRow>
                                <TableCell>Odometer</TableCell>
                                <TableCell>:</TableCell>
                                <TableCell>Harley Davidson</TableCell>
                              </TableRow>
                              <TableRow>
                                <TableCell>Weight</TableCell>
                                <TableCell>:</TableCell>
                                <TableCell>Harley Davidson</TableCell>
                              </TableRow>
                            </TableBody>
                          </Table>
                        </Grid.Column>
                        <Grid.Column>
                          <Segment>
                            <span>Share : </span>
                            <Button circular color="facebook" icon="facebook" />
                            <Button circular color="twitter" icon="twitter" />
                            <Button circular color="linkedin" icon="linkedin" />
                            <Button
                              circular
                              color="google plus"
                              icon="google plus"
                            />
                          </Segment>
                          <Segment>
                            TIPS :: Text test Text test Text test Text test Text
                            test Text test Text test Text test Text test Text
                            test Text test{" "}
                          </Segment>

                          <div>
                            <Button.Group widths="2">
                              <Button
                                color="blue"
                                floated="left"
                                content="Location"
                              >
                                <Icon name="map marker alternate" />
                                <p>Bandung Jawa Barat</p>
                              </Button>
                              <Button
                                color="google plus"
                                icon="upload"
                                floated="right"
                              >
                                <Icon name="phone square" size="large" />
                                <p>08132188778</p>
                              </Button>
                            </Button.Group>
                          </div>
                        </Grid.Column>
                      </Grid>
                    </Item.Content>
                  </Item>
                </Item.Group>
              </Grid.Column>
            </Grid>
          </Grid.Column>
          <Grid.Column />
        </Grid>
      </div>
    );
  }
}
export default ProductDetail;
