import React, { Component } from "react";
import {
  Grid,
  Image,
  Segment,
  Advertisement,
  Card,
  Button,
  Icon,
  Header,
  Label
} from "semantic-ui-react";
import { Redirect } from "react-router";

class Motorcycle extends Component {
  constuctor() {
    this.routeChange = this.routeChange.bind(this);
  }
  state = {
    navigate: false
  };
  routeChange(e) {
    e.preventDefault();
    this.context.router.transitionTo("/loginfb");
  }
  componentDidMount() {
    window.addEventListener("hashchange", this.routeChange, false);
  }
  render() {
    const { navigate } = this.state;

    // here is the important part
    if (navigate) {
      return <Redirect to="/product" push={true} />;
    }
    return (
      <Grid columns="equal">
        <Grid.Column />
        <Grid.Column width={12}>
          <Grid>
            <Grid.Column width={14}>
              <Header as="h3">
                <Label as="a" color="black" ribbon>
                  <Icon name="cog" />
                  <Header.Content>Suggest Motorcycle</Header.Content>
                </Label>
              </Header>
              <Card.Group>
                <Card>
                  <Image src="/assets/img/suggest1.jpg" />
                  <Card.Content>
                    <Card.Header>FS Triumph Daytona 500cc 1969</Card.Header>

                    <Label attached="top right" size="medium" color="teal" tag>
                      Used
                    </Label>
                    <Card.Description>
                      <Label size="huge">Rp.110.000.000</Label>
                      <div>
                        <Icon floated="right" name="map marker alternate" />
                        Bandung
                      </div>
                    </Card.Description>
                  </Card.Content>
                  <Card.Content extra>
                    <div>
                      <a>
                        <Icon name="user" />
                        10 View
                      </a>

                      <Button
                        floated="right"
                        basic
                        color="blue"
                        onClick={() => this.setState({ navigate: true })}
                      >
                        More Detail
                      </Button>
                    </div>
                  </Card.Content>
                </Card>
                <Card>
                  <Image src="/assets/img/suggest2.jpg" />
                  <Card.Content>
                    <Card.Header>For Sale Sportster 1200 cc 2004</Card.Header>

                    <Label attached="top right" size="medium" color="teal" tag>
                      Used
                    </Label>
                    <Card.Description>
                      <Label size="huge">Rp.140.000.000</Label>
                      <div>
                        <Icon floated="right" name="map marker alternate" />
                        Bandung
                      </div>
                    </Card.Description>
                  </Card.Content>
                  <Card.Content extra>
                    <div>
                      <a>
                        <Icon name="user" />
                        103 View
                      </a>

                      <Button
                        floated="right"
                        basic
                        color="blue"
                        onClick={() => this.setState({ navigate: true })}
                      >
                        More Detail
                      </Button>
                    </div>
                  </Card.Content>
                </Card>
                <Card>
                  <Image src="/assets/img/suggest3.jpg" />
                  <Card.Content>
                    <Card.Header>
                      FS HD harley Shovelhead 1974 mulus
                    </Card.Header>

                    <Label attached="top right" size="medium" color="teal" tag>
                      Used
                    </Label>
                    <Card.Description>
                      <Label size="huge">Rp.225.000.000</Label>
                      <div>
                        <Icon floated="right" name="map marker alternate" />
                        Bandung
                      </div>
                    </Card.Description>
                  </Card.Content>
                  <Card.Content extra>
                    <div>
                      <a>
                        <Icon name="user" />
                        1006 View
                      </a>

                      <Button
                        floated="right"
                        basic
                        color="blue"
                        onClick={() => this.setState({ navigate: true })}
                      >
                        More Detail
                      </Button>
                    </div>
                  </Card.Content>
                </Card>
              </Card.Group>
            </Grid.Column>
            <Grid.Column width={2}>
              <Advertisement unit="wide skyscraper" test="Advertise" />
            </Grid.Column>
          </Grid>
        </Grid.Column>
        <Grid.Column />
      </Grid>
    );
  }
}

export default Motorcycle;
