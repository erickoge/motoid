import React from "react";
import {
  Grid,
  Image,
  Segment,
  Advertisement,
  Card,
  Button,
  Icon,
  Header,
  Label
} from "semantic-ui-react";

const Sparepart = () => (
  <Grid columns="equal">
    <Grid.Column />
    <Grid.Column width={12}>
      <Header as="h3">
        <Label as="a" color="black" ribbon>
          <Icon name="cog" />
          <Header.Content>Suggest Spareparts</Header.Content>
        </Label>
      </Header>
      <Card.Group>
        <Card>
          <Image src="/assets/img/sparepart1.jpg" />
          <Card.Content>
            <Card.Header>Spare Part Harley Davidson</Card.Header>

            <Label attached="top right" size="medium" color="teal" tag>
              Used
            </Label>
            <Card.Description>
              <Label size="huge">Rp.225.000.000</Label>
              <div>
                <Icon floated="right" name="map marker alternate" />
                Bandung
              </div>
            </Card.Description>
          </Card.Content>
          <Card.Content extra>
            <div>
              <a>
                <Icon name="user" />
                1006 View
              </a>

              <Button
                floated="right"
                basic
                color="blue"
                onClick={() => this.setState({ navigate: true })}
              >
                More Detail
              </Button>
            </div>
          </Card.Content>
        </Card>
        <Card>
          <Image src="/assets/img/sparepart2.jpg" />
          <Card.Content>
            <Card.Header>Spare Part Harley Davidson</Card.Header>

            <Label attached="top right" size="medium" color="teal" tag>
              Used
            </Label>
            <Card.Description>
              <Label size="huge">Rp.225.000.000</Label>
              <div>
                <Icon floated="right" name="map marker alternate" />
                Bandung
              </div>
            </Card.Description>
          </Card.Content>
          <Card.Content extra>
            <div>
              <a>
                <Icon name="user" />
                1006 View
              </a>

              <Button
                floated="right"
                basic
                color="blue"
                onClick={() => this.setState({ navigate: true })}
              >
                More Detail
              </Button>
            </div>
          </Card.Content>
        </Card>
        <Card>
          <Image src="/assets/img/sparepart3.jpg" />
          <Card.Content>
            <Card.Header>Spare Part Harley Davidson</Card.Header>

            <Label attached="top right" size="medium" color="teal" tag>
              Used
            </Label>
            <Card.Description>
              <Label size="huge">Rp.225.000.000</Label>
              <div>
                <Icon floated="right" name="map marker alternate" />
                Bandung
              </div>
            </Card.Description>
          </Card.Content>
          <Card.Content extra>
            <div>
              <a>
                <Icon name="user" />
                1006 View
              </a>

              <Button
                floated="right"
                basic
                color="blue"
                onClick={() => this.setState({ navigate: true })}
              >
                More Detail
              </Button>
            </div>
          </Card.Content>
        </Card>
      </Card.Group>
    </Grid.Column>
    <Grid.Column />
  </Grid>
);

export default Sparepart;
