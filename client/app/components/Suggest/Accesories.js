import React from "react";
import {
  Grid,
  Image,
  Segment,
  Advertisement,
  Card,
  Button,
  Icon,
  Header,
  Label
} from "semantic-ui-react";

const Accesories = () => (
  <Grid columns="equal">
    <Grid.Column />
    <Grid.Column width={12}>
      <Header as="h3">
        <Label as="a" color="black" ribbon>
          <Icon name="cog" />
          <Header.Content>Suggest Accesories</Header.Content>
        </Label>
      </Header>
      <Card.Group>
        <Card>
          <Image src="/assets/img/accesories1.jpg" />
          <Card.Content>
            <Card.Header>Accesories Harley Davidson</Card.Header>

            <Label attached="top right" size="medium" color="teal" tag>
              Used
            </Label>
            <Card.Description>
              <Label size="huge">Rp.15.000.000</Label>
              <div>
                <Icon floated="right" name="map marker alternate" />
                Bandung
              </div>
            </Card.Description>
          </Card.Content>
          <Card.Content extra>
            <div>
              <a>
                <Icon name="user" />
                106 View
              </a>

              <Button
                floated="right"
                basic
                color="blue"
                onClick={() => this.setState({ navigate: true })}
              >
                More Detail
              </Button>
            </div>
          </Card.Content>
        </Card>
        <Card>
          <Image src="/assets/img/accesories1.jpg" />
          <Card.Content>
            <Card.Header>Accesories Harley Davidson</Card.Header>

            <Label attached="top right" size="medium" color="teal" tag>
              Used
            </Label>
            <Card.Description>
              <Label size="huge">Rp.35.000.000</Label>
              <div>
                <Icon floated="right" name="map marker alternate" />
                Bandung
              </div>
            </Card.Description>
          </Card.Content>
          <Card.Content extra>
            <div>
              <a>
                <Icon name="user" />
                16 View
              </a>

              <Button
                floated="right"
                basic
                color="blue"
                onClick={() => this.setState({ navigate: true })}
              >
                More Detail
              </Button>
            </div>
          </Card.Content>
        </Card>
        <Card>
          <Image src="/assets/img/accesories1.jpg" />
          <Card.Content>
            <Card.Header>Accesories Harley Davidson</Card.Header>

            <Label attached="top right" size="medium" color="teal" tag>
              Used
            </Label>
            <Card.Description>
              <Label size="huge">Rp.25.000.000</Label>
              <div>
                <Icon floated="right" name="map marker alternate" />
                Bandung
              </div>
            </Card.Description>
          </Card.Content>
          <Card.Content extra>
            <div>
              <a>
                <Icon name="user" />
                209 View
              </a>

              <Button
                floated="right"
                basic
                color="blue"
                onClick={() => this.setState({ navigate: true })}
              >
                More Detail
              </Button>
            </div>
          </Card.Content>
        </Card>
      </Card.Group>
    </Grid.Column>
    <Grid.Column />
  </Grid>
);

export default Accesories;
