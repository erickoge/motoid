import React, { Component } from "react";
import {
  Grid,
  GridColumn,
  Segment,
  Form,
  List,
  Input,
  Button
} from "semantic-ui-react";
import Upload from "./Upload";
import { equal } from "assert";

export default class DynamicSelect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      kategoris: [
        {
          name: "level1",
          data: [
            {
              key: "1",
              text: "American Motorcycle",
              value: "American Motorcycle"
            },
            {
              key: "2",
              text: "European Motorcycle",
              value: "European Motorcycle"
            },
            {
              key: "3",
              text: "Japanese Motorcycle",
              value: "Japanese Motorcycle"
            }
          ]
        },
        { name: "level2" },
        { name: "level3" }
      ],
      dataLvl1: [],
      dataLvl11: [
        {
          key: "1",
          text: "American Motorcycle",
          value: "American Motorcycle"
        },
        {
          key: "2",
          text: "European Motorcycle",
          value: "European Motorcycle"
        },
        {
          key: "3",
          text: "Japanese Motorcycle",
          value: "Japanese Motorcycle"
        }
      ],
      dataLvl2: "",
      dataLvl3: "",
      dropdown1: false,
      dropdown2: true,
      dropdown3: true,
      dropdown4: true,
      dropdown5: true,
      valueLvl1: "",
      valueLvl2: "",
      valueLvl3: "",
      valueLvl4: "",
      valueLvl5: ""
    };
    this.onChangeValueLvl1 = this.onChangeValueLvl1.bind(this);
    this.onChangeValueLvl2 = this.onChangeValueLvl2.bind(this);
    this.onChangeValueLvl3 = this.onChangeValueLvl3.bind(this);
  }

  componentDidMount() {
    let dataku = "";
    let arr = [];
    var arrSend = ["American Motorcycle", "Motorcycle"];
    fetch("/api/data/kategoriku", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },

      body: JSON.stringify({
        ancestors1: ""
      })
    })
      //fetch("/api/test/kategori?parent=")
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          Object.keys(json.data).forEach(function(key) {
            dataku = {
              key: key,
              text: json.data[key].name,
              value: json.data[key].name
            };
            arr.push(dataku);
          });
          // console.log(json);
          this.setState({
            dataLvl1: arr
          });
        } else {
          console.log(" teu sukses");
        }
      });
  }

  onChangeValueLvl1 = (e, { value }) => {
    // alert(value);
    parent = value;
    let arr = [];

    this.setState({
      valueLvl1: parent
    });
    this.setState({
      dataLvl2: arr
    });
    let dataku = "";
    //fetch("/api/data/kategoris?parent=" + parent)
    fetch("/api/data/kategoriku", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },

      body: JSON.stringify({
        ancestors1: parent
      })
    })
      .then(res => res.json())
      .then(json => {
        console.log(json.data);

        if (json.success) {
          Object.keys(json.data).forEach(function(key) {
            dataku = {
              key: key,
              text: json.data[key].name,
              value: json.data[key].name
            };
            arr.push(dataku);
          });
          this.setState({
            dataLvl2: arr
          });
          this.setState({
            dropdown2: false
          });
          this.setState({
            dropdown3: true
          });
        } else {
          /* this.setState({
            isLoading: false
          });*/
        }
      });
    // this.setState({ gender: value });
  };
  onChangeValueLvl2 = (e, { value }) => {
    //alert(value);
    parent = value;
    let arr2 = [];
    this.setState({
      valueLvl2: value
    });
    this.setState({
      dataLvl3: arr2
    });
    let dataku = "";
    //fetch("/api/data/kategoris?parent=" + parent)
    fetch("/api/data/kategoriku", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },

      body: JSON.stringify({
        ancestors1: this.state.valueLvl1,
        ancestors2: parent
      })
    })
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          Object.keys(json.data).forEach(function(key) {
            dataku = {
              key: key,
              text: json.data[key].name,
              value: json.data[key].name
            };
            arr2.push(dataku);
          });
          this.setState({
            dataLvl3: arr2
          });
          this.setState({
            dropdown3: false
          });
        }
      });
  };

  onChangeValueLvl3 = (e, { value }) => {
    parent = value;
    let arr3 = [];
    this.setState({
      valueLvl3: parent
    });
    this.setState({
      dataLvl4: arr3
    });
    let dataku = "";
    //fetch("/api/data/kategoris?parent=" + parent)
    fetch("/api/data/kategoriku", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },

      body: JSON.stringify({
        ancestors1: this.state.valueLvl1,
        ancestors2: this.state.valueLvl2,
        ancestors3: parent
      })
    })
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          Object.keys(json.data).forEach(function(key) {
            dataku = {
              key: key,
              text: json.data[key].name,
              value: json.data[key].name
            };
            arr3.push(dataku);
          });
          this.setState({
            dataLvl4: arr3
          });
          this.setState({
            dropdown4: false
          });
        }
      });
  };
  onChangeValueLvl4 = (e, { value }) => {};
  onChangeValueLvl5 = (e, { value }) => {
    this.setState({
      valueLvl5: value
    });
  };
  onChangeValuexx(event) {
    //get data from server base on value
    // alert("event.target.value" + event.target.value);
    parent = event.target.value;
    fetch("/api/data/kategoris?parent=" + parent)
      .then(res => res.json())
      .then(json => {
        console.log(json);
        if (json.success) {
          this.setState({
            token,
            isLoading: false
          });
        } else {
          this.setState({
            isLoading: false
          });
        }
      });
  }

  render() {
    const genderOptions = "";
    const kondisiOptions = [
      { key: "1", text: "Baru", value: "Baru" },
      { key: "2", text: "Bekas", value: "Bekas" },
      { key: "3", text: "Rekondisi", value: "Rekondisi" }
    ];
    return (
      <div style={{ marginTop: "10.3em" }}>
        <Grid columns="equal">
          <GridColumn />
          <GridColumn width={10}>
            <Segment>
              <h4>Sell Items on Motostation</h4>
            </Segment>
            <Segment>
              <Form>
                <Form.Group />
                <Form.Group>
                  <Form.Select
                    width={5}
                    name="field1"
                    options={this.state.dataLvl1}
                    label="Category"
                    onChange={this.onChangeValueLvl1}
                  />
                  <Form.Select
                    width={5}
                    name="field2"
                    options={this.state.dataLvl2}
                    label="Sub Category"
                    onChange={this.onChangeValueLvl2}
                    disabled={this.state.dropdown2}
                  />
                  <Form.Select
                    width={5}
                    name="field3"
                    options={this.state.dataLvl3}
                    label="BrandstestRUbah"
                    onChange={this.onChangeValueLvl3}
                    disabled={this.state.dropdown3}
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Select
                    width={5}
                    name="field4"
                    options={this.state.dataLvl4}
                    label="Brand"
                    onChange={this.onChangeValueLvl4}
                    disabled={this.state.dropdown4}
                  />
                  <Form.Select
                    width={5}
                    name="field5"
                    options={this.state.dataLvl5}
                    label="Type"
                    onChange={this.onChangeValueLvl5}
                    disabled={this.state.dropdown5}
                  />
                </Form.Group>
              </Form>
            </Segment>
            <Segment>
              <Form>
                <Form.Field
                  id="form-input-control-first-name"
                  control={Input}
                  label="JUDUL IKLAN"
                  placeholder="Judul Iklan"
                />
                <label>UPLOAD PHOTO</label>
                <Segment>
                  <List horizontal>
                    <List.Item>
                      <Upload />
                    </List.Item>
                    <List.Item>
                      <Upload />
                    </List.Item>
                    <List.Item>
                      <Upload />
                    </List.Item>
                    <List.Item>
                      <Upload />
                    </List.Item>
                    <List.Item>
                      <Upload />
                    </List.Item>
                  </List>
                </Segment>
                <Form.Group widths="equal">
                  <Form.Select
                    name="fieldGender"
                    options={kondisiOptions}
                    label="Kondisi"
                    placeholder="Gender"
                    defaultValue={"Baru"}
                  />
                  <Form.Field
                    id="form-input-control-first-name"
                    control={Input}
                    label="Tahun Pembuatan"
                    placeholder="Judul Iklan"
                  />
                  <Form.Field
                    id="form-input-control-first-name"
                    control={Input}
                    label="Warna"
                    placeholder="Judul Iklan"
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <Form.Select
                    name="fieldGender"
                    options={kondisiOptions}
                    label="Kapasitas Mesin"
                    placeholder="Gender"
                    defaultValue={"Baru"}
                  />
                  <Form.Field
                    id="form-input-control-first-name"
                    control={Input}
                    label="Odometer"
                    placeholder="Judul Iklan"
                  />
                  <Form.Field
                    id="form-input-control-first-name"
                    control={Input}
                    label="Berat(Kg)"
                    placeholder="Judul Iklan"
                  />
                  <Form.Field
                    id="form-input-control-first-name"
                    control={Input}
                    label="Quantity"
                    placeholder="Judul Iklan"
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <Form.Select
                    name="fieldGender"
                    options={kondisiOptions}
                    label="Lokasi"
                    placeholder="Gender"
                    defaultValue={"Baru"}
                  />
                  <Form.Field
                    id="form-input-control-first-name"
                    control={Input}
                    label="Kota"
                    placeholder="Judul Iklan"
                  />
                  <Form.Field
                    id="form-input-control-first-name"
                    control={Input}
                    label="No. HP"
                    placeholder="Judul Iklan"
                  />
                </Form.Group>
                <Button type="submit" positive>
                  Simpan
                </Button>
                <Button>Cancel</Button>
              </Form>
            </Segment>
          </GridColumn>
          <GridColumn />
        </Grid>
      </div>
    );
  }
}
/*
{this.state.kategoris.map((kategori, idx) => (
                  
                ))}
                */
