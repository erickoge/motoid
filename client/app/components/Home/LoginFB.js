import React, { Component } from "react";
import FacebookLogin from "react-facebook-login";
import FacebookLoginButton from "../Login/LoginButtonFB";
//import GoogleLogin from 'react-google-login';
/*
<GoogleLogin
        clientId="" //CLIENTID NOT CREATED YET
        buttonText="LOGIN WITH GOOGLE"
        onSuccess={responseGoogle}
        onFailure={responseGoogle}
      />
      */
class LoginFB extends Component {
  state = {
    username: null
  };
  onFacebookLogin = (loginStatus, resultObject) => {
    if (loginStatus === true) {
      this.setState({
        username: resultObject.user.name
      });
    } else {
      alert("Facebook login error");
    }
  };
  render() {
    const { username } = this.state;
    console.log("Load Page");
    const responseFacebook = response => {
      console.log(response);
    };

    const responseGoogle = response => {
      console.log(response);
    };
    /*
<FacebookLogin
          appId="2093838757597709" //APP ID NOT CREATED YET
          fields="name,email,picture"
          callback={responseFacebook}
        />
        */
    return (
      <div className="App">
        <h1>LOGIN WITH FACEBOOK AND GOOGLE</h1>

        <br />
        <br />

        <div className="App-intro">
          {!username && (
            <div>
              <p>Click on one of any button below to login</p>
              <FacebookLoginButton onLogin={this.onFacebookLogin}>
                <button>Facebook</button>
              </FacebookLoginButton>
            </div>
          )}
          {username && <p>Welcome back, {username}</p>}
        </div>
      </div>
    );
  }
}

export default LoginFB;
