import React, { Component } from "react";

export default class FacebookLogin extends Component {
  componentDidMount() {
    (function(d, s, id) {
      console.log("in function");
      var js,
        fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      console.log(id);
      js = d.createElement(s);
      js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    })(document, "script", "facebook-jssdk");
    window.fbAsyncInit = function() {
      FB.init({
        appId: "2093838757597709",
        autoLogAppEvents: true,
        xfbml: true,
        version: "v2.1"
      });

      FB.Event.subscribe(
        "auth.statusChange",
        function(response) {
          console.log("in function1");
          if (response.authResponse) {
            this.updateLoginState(response);
            console.log(response);
          } else {
            // this.updateLogoutState();
            console.log("logout");
          }
        }.bind(this)
      );
    }.bind(this);
  }
  updateLoginState = response => {
    if (response.status === "connected") {
      this.FB.api("/me", userdata => {
        let result = {
          ...response,
          user: userdata
        };
        this.props.onLogin(true, result);
      });
    } else {
      this.props.onLogin(false);
    }
  };

  //
  /**
   * Init FB object and check Facebook Login status
   */
  initializeFacebookLogin = () => {
    this.FB = window.FB;
    this.checkLoginStatus();
  };

  /**
   * Check login status
   */
  checkLoginStatus = () => {
    this.FB.getLoginStatus(this.facebookLoginHandler);
  };

  /**
   * Check login status and call login api is user is not logged in
   */
  facebookLogin = () => {
    if (!this.FB) {
      console.log("FB Not Init!!");
      return;
    }

    this.FB.getLoginStatus(response => {
      if (response.status === "connected") {
        this.facebookLoginHandler(response);
      } else {
        this.FB.login(this.facebookLoginHandler, { scope: "public_profile" });
      }
    });
  };

  /**
   * Handle login response
   */
  facebookLoginHandler = response => {
    if (response.status === "connected") {
      this.FB.api("/me", userData => {
        let result = {
          ...response,
          user: userData
        };
        this.props.onLogin(true, result);
      });
    } else {
      this.props.onLogin(false);
    }
  };

  render() {
    let { children } = this.props;
    return <div onClick={this.facebookLogin}>{children}</div>;
  }
  //
}
