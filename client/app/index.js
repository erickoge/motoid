import React from "react";
import { render } from "react-dom";

import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

import App from "./components/App/App";
import NotFound from "./components/App/NotFound";

import Home from "./components/Home/Home";

import HelloWorld from "./components/HelloWorld/HelloWorld";
import LoginFB from "./components/Home/LoginFB";
import LoginButtonFB from "./components/Login/LoginButtonFB";
import "./styles/styles.scss";
import ProdukDetail from "./components/Page/ProductDetail";
import Categories from "./components/Page/Categories";
import Users from "./components/Page/Users";
import Profile from "./components/Page/User/Profile";
import Signup from "./components/Page/User/Signup";
import Sell from "./components/Page/Sell";
import SellFields from "./components/Page/SellFields";
import IncorporationForm from "./components/Test/DynamicInput";
import DynamicSelect from "./components/Test/DynamicSelect";
import UserEdit from "./components/Page/User/UserEdit";
import WatchList from "./components/Page/User/watchlist";
import ItemSale from "./components/Page/User/ItemSale";
import ChangePass from "./components/Page/User/ChangePass";
import SearchResult from "./components/Page/Product/search";
import NewProduct from "./components/Page/Product/newProduct";
import NewProduct2 from "./components/Page/Product/newProduct2";
render(
  <Router>
    <App>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/helloworld" component={HelloWorld} />
        <Route path="/loginfb" component={LoginFB} />
        <Route path="/product" component={ProdukDetail} />
        <Route path="/categories" component={Categories} />
        <Route path="/users" component={Users} />
        <Route path="/user/profile/:token" component={Profile} />
        <Route path="/signup" component={Signup} />
        <Route path="/sell" component={Sell} />
        <Route path="/sellfields" component={SellFields} />
        <Route path="/test/dinamicinput" component={IncorporationForm} />
        <Route path="/test/dynamicselect" component={DynamicSelect} />
        <Route path="/user/edit" component={UserEdit} />
        <Route path="/user/watchlist" component={WatchList} />
        <Route path="/user/itemsale" component={ItemSale} />
        <Route path="/user/changepass" component={ChangePass} />
        <Route path="/produk/search/" component={SearchResult} />
        <Route
          path="/produk/search/:token/:searchKey"
          component={SearchResult}
        />
        <Route path="/produk/new/:token" component={NewProduct} />
        <Route path="/produk/new2/:token/:productId" component={NewProduct2} />
        <Route component={NotFound} />
      </Switch>
    </App>
  </Router>,
  document.getElementById("app")
);
