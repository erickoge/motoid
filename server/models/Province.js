const mongoose = require("mongoose");
const ProvinceSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: "Name is required"
  },
  updated: Date,
  created: {
    type: Date,
    default: Date.now
  }
});
module.exports = mongoose.model("Province", ProvinceSchema);
