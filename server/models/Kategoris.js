const mongoose = require("mongoose");

const KategorisSchema = new mongoose.Schema({
  name: {
    type: String,
    default: ""
  },
  parent: {
    type: String,
    default: ""
  }
});
module.exports = mongoose.model("Kategoris", KategorisSchema);
