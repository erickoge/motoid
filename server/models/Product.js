const mongoose = require("mongoose");
const ProductSchema = new mongoose.Schema({
  title: {
    type: String,
    trim: true,
    required: "Title is required",
    index: true
  },
  tag: {
    type: String,
    trim: true,
    index: true
  },
  image: {
    data: Buffer,
    contentType: String
  },
  yearout: {
    type: Number
  },
  color: {
    type: String
  },
  condition: { type: String, trim: true },
  description: {
    type: String,
    trim: true
  },
  quantity: {
    type: String
  },
  enginecc: {
    type: Number
  },
  price: {
    type: Number,
    required: "Price is required"
  },
  kilometer: {
    type: Number
  },
  phone: {
    type: String
  },
  desc: {
    type: String
  },
  file1: {
    data: Buffer,
    contentType: String
  },
  file2: {
    data: Buffer,
    contentType: String
  },
  file3: {
    data: Buffer,
    contentType: String
  },
  file4: {
    data: Buffer,
    contentType: String
  },
  file5: {
    data: Buffer,
    contentType: String
  },
  updated: Date,
  created: {
    type: Date,
    default: Date.now
  },
  kategoris: { type: mongoose.Schema.ObjectId, ref: "Kategoris" },
  city: { type: mongoose.Schema.ObjectId, ref: "Kabkota" },
  province: { type: mongoose.Schema.ObjectId, ref: "Province" },
  user: { type: mongoose.Schema.ObjectId, ref: "User" }
});
module.exports = mongoose.model("Product", ProductSchema);
