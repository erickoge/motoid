const mongoose = require("mongoose");
const KabkotaSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: "Name is required"
  },
  updated: Date,
  created: {
    type: Date,
    default: Date.now
  },
  province: { type: mongoose.Schema.ObjectId, ref: "Province" }
});
module.exports = mongoose.model("Kabkota", KabkotaSchema);
