const User = require("../../models/User");
const path = require("path");
const multer = require("multer");
const UserSession = require("../../models/UserSession");

const storagex = multer.diskStorage({
  destination: "./client/public/xxx/",
  filename: (req, file, cb) => {
    console.log(file);
    cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname);
  }
});
const upload = multer({
  storage: storagex,
  limits: { fileSize: 1000000000 }
}).single("myImage");

/*
const upload = multer({ storage: storagex });

filename: function(req, file, cb) {
   
    cb(null, "IMAGE-" + Date.now() + path.extname(file.originalname));
  }

*/

module.exports = app => {
  /*
upload file
*/
  app.post("/api/img/produk", upload, (req, res) => {
    return res.send({
      success: true,
      filename: req.file.filename
    });
  });

  app.get("/api/account/userdata", (req, res, next) => {
    // Get the token
    const { query } = req;
    const { token } = query;
    console.log("token" + token);
    UserSession.find(
      {
        _id: token
      },
      (err, users) => {
        if (err) {
          console.log("err 2:", err);
          return res.send({
            success: false,
            message: "Error: server error"
          });
        }
        if (users.length != 1) {
          return res.send({
            success: false,
            message: "Error: Token Not Found "
          });
        }
        const user = users[0];
        User.find(
          {
            _id: user.userId
          },
          (err, datas) => {
            if (err) {
              console.log("err 2:", err);
              return res.send({
                success: false,
                message: "Error: server error"
              });
            }
            if (datas.length != 1) {
              return res.send({
                success: false,
                message: "Error: User Not Found "
              });
            } else {
              return res.send({
                success: false,
                message: "Success",
                data: datas
              });
            }
          }
        );

        /*User.findById(token)
      .exec()
      .then(user => res.json(user));*/
      }
    );
  });

  /*
   * Sign up
   */

  app.post("/api/account/register", (req, res, next) => {
    const { body } = req;
    const { password1 } = body;
    const { firstName } = body;
    let { email } = body;

    const { lastName } = body;
    const { gender } = body;
    const { city } = body;
    const { dateBirth } = body;
    const { cityBirth } = body;
    const { phone } = body;

    if (!email) {
      return res.send({
        success: false,
        message: "Error: Email cannot be blank."
      });
    }
    if (!password1) {
      return res.send({
        success: false,
        message: "Error: Password cannot be blank."
      });
    }
    email = email.toLowerCase();
    email = email.trim();
    // Steps:
    // 1. Verify email doesn't exist
    // 2. Save
    User.find(
      {
        email: email
      },
      (err, previousUsers) => {
        if (err) {
          return res.send({
            success: false,
            message: "Error: Server error find user"
          });
        } else if (previousUsers.length > 0) {
          return res.send({
            success: false,
            message: "Error: Account already exist."
          });
        }
        // Save the new user
        const newUser = new User();
        newUser.firstName = firstName;
        newUser.lastName = lastName;
        newUser.cityBirth = cityBirth;
        newUser.birthDate = dateBirth;
        newUser.city = city;
        newUser.phone = phone;
        newUser.email = email;
        console.log("Password : " + newUser.generateHash(password1));
        newUser.password = newUser.generateHash(password1);
        newUser.gender = gender;
        newUser.save((err, user) => {
          if (err) {
            console.log("Error Server : " + err);
            return res.send({
              success: false,
              message: "Error: Server error on save"
            });
          }
          return res.send({
            success: true,
            message: "Signed up"
          });
        });
      }
    );
  }); // end of sign up endpoint
};
