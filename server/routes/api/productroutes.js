const express = require("express");
const productCtrl = require("../../controllers/product-controller");
const userCtrl = require("../../controllers/user-controller");

const router = express.Router();
module.exports = router => {
  // suggest motorcycle
  router.route("/api/suggestmotor").get(productCtrl.suggestMotor);

  //search
  router.route("/api/products").get(productCtrl.list);

  router.route("/api/products/by/userId/:userId").get(productCtrl.listByuserId);
  router.route("/api/products/by/:token").post(productCtrl.create);
  router.route("/api/products/by2/:token/:productId").put(productCtrl.create2);
  // .get(productCtrl.listByShop);

  //router.param("token", userCtrl.addReqToken);
  //router.param("kategoris", katCtrl.addReqKat);
  router.route("/api/produk/db/province").get(productCtrl.listProvince);
  router.route("/api/produk/db/city/:provId").get(productCtrl.listCity);

  router
    .route("/api/product/image/:productId")
    .get(productCtrl.photo, productCtrl.defaultPhoto);

  //params
  router.param("token", userCtrl.userID);
  router.param("provId", (req, res, next, id) => {
    // console.log("id=> " + id);
    req.provId = id;
    next();
  });
  router.param("productId", productCtrl.productByID);
};
