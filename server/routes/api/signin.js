const User = require("../../models/User");
const UserSession = require("../../models/UserSession");
const Kategoris = require("../../models/Kategoris");

const express = require("express");
//const fileUpload = require("express-fileupload");
var ObjectId = require("mongodb").ObjectID;

module.exports = app => {
  /*Uploads Images */
  //app.use(fileUpload({ safeFileNames: true, preserveExtension: true }));

  app.post("/api/image/upload", (req, res, next) => {
    const { query } = req;
    const { body } = query;
    console.log(body);
    /*body.mv("${__dirname}/public/assets/img/${fileName}", function(err) {
      if (err) {
        return res.status(500).send(err);
      }

      res.json({
        file: `public/assets/img/${body.name}`
      });
    });
    */
  });

  /*
kategoris
*/
  app.post("/api/data/kategoriku", (req, res, next) => {
    console.log("Call from client");
    const { body } = req;
    let { ancestors1 } = body;
    let { ancestors2 } = body;
    let { ancestors3 } = body;
    let { ancestors4 } = body;

    let arr = [];
    // arr.push("");
    if (ancestors1 !== "") {
      arr.push(ancestors1);
      if (typeof ancestors2 !== "undefined") {
        arr.push(ancestors2);
        if (typeof ancestors3 !== "undefined") {
          arr.push(ancestors3);
          if (typeof ancestors4 !== "undefined") {
            arr.push(ancestors4);
          }
        }
      }
    }

    console.log("arr:" + arr + " ancestors1:" + ancestors1);
    //if (!parent) parent = "";
    //Kategoris.find(ObjectId(parent), (err, kategori) => {
    Kategoris.find({ ancestors: arr }, (err, kategori) => {
      if (err) {
        return res.send({
          success: false,
          message: "Error: server error"
        });
      }
      //console.log(kategori);
      return res.send({
        success: true,
        data: kategori
      });
    });
  });

  app.get("/api/data/kategoris", (req, res, next) => {
    const { query } = req;
    const { parent } = query;
    //console.log("parent 1:" + parent);

    Kategoris.find({ parent: parent }, (err, kategori) => {
      if (err) {
        return res.send({
          success: false,
          message: "Error: server error"
        });
      }

      if (kategori.length > 1) {
        /*
        newKategori.name = email;
        newKategori.parent = newUser.generateHash(password);
        newUser.save((err, user) => {
          if (err) {
            return res.send({
              success: false,
              message: "Error: Server error"
            });
          }
          return res.send({
            success: true,
            message: "Signed up"
          });
          
        });*/

        return res.send({
          success: true,
          message: "OK",
          data: kategori
        });
      } else {
        //create new kategoris collection + data
        /* const data = [
          {
            key: "1",
            text: "American Motorcycle",
            value: "American Motorcycle"
          },
          {
            key: "2",
            text: "European Motorcycle",
            value: "European Motorcycle"
          },
          {
            key: "3",
            text: "Japanese Motorcycle",
            value: "Japanese Motorcycle"
          }
        ];

        const newKategoris1 = new Kategoris();
        newKategoris1.name = "American Motorcycle";
        newKategoris1.save();

        const newKategoris2 = new Kategoris();
        newKategoris2.name = "European Motorcycle";
        newKategoris2.save();

        const newKategoris3 = new Kategoris();
        newKategoris3.name = "Japanese Motorcycle";
        newKategoris3.save();
        return res.send({
          success: true,
          message: "data empty, create Collection Kategoris",
          data: kategori
        });
        */
      }
    });
  });
  /*
   * Sign up
   */
  app.post("/api/account/signup", (req, res, next) => {
    const { body } = req;
    const { password } = body;
    let { email } = body;

    if (!email) {
      return res.send({
        success: false,
        message: "Error: Email cannot be blank."
      });
    }
    if (!password) {
      return res.send({
        success: false,
        message: "Error: Password cannot be blank."
      });
    }
    email = email.toLowerCase();
    email = email.trim();
    // Steps:
    // 1. Verify email doesn't exist
    // 2. Save
    User.find(
      {
        email: email
      },
      (err, previousUsers) => {
        if (err) {
          return res.send({
            success: false,
            message: "Error: Server error"
          });
        } else if (previousUsers.length > 0) {
          return res.send({
            success: false,
            message: "Error: Account already exist."
          });
        }
        // Save the new user
        const newUser = new User();
        newUser.email = email;
        newUser.password = newUser.generateHash(password);
        newUser.save((err, user) => {
          if (err) {
            return res.send({
              success: false,
              message: "Error: Server error"
            });
          }
          return res.send({
            success: true,
            message: "Signed up"
          });
        });
      }
    );
  }); // end of sign up endpoint

  app.post("/api/account/signin", (req, res, next) => {
    const { body } = req;
    const { password } = body;
    let { email } = body;

    if (!email) {
      return res.send({
        success: false,
        message: "Error: Email cannot be blank."
      });
    }
    if (!password) {
      return res.send({
        success: false,
        message: "Error: Password cannot be blank."
      });
    }

    email = email.toLowerCase();
    email = email.trim();

    User.find(
      {
        email: email
      },
      (err, users) => {
        if (err) {
          console.log("err 2:", err);
          return res.send({
            success: false,
            message: "Error: server error"
          });
        }
        if (users.length != 1) {
          return res.send({
            success: false,
            message: "Error: Invalid Username "
          });
        }

        const user = users[0];
        if (!user.validPassword(password)) {
          return res.send({
            success: false,
            message: "Error: Invalid Password"
          });
        }

        // Otherwise correct user
        const userSession = new UserSession();
        userSession.userId = user._id;
        userSession.save((err, doc) => {
          if (err) {
            console.log(err);
            return res.send({
              success: false,
              message: "Error: server error"
            });
          }

          return res.send({
            success: true,
            message: "Valid sign in",
            token: doc._id
          });
        });
      }
    );
  });
  app.get("/api/account/verify", (req, res, next) => {
    // Get the token
    const { query } = req;
    const { token } = query;
    // ?token=test

    // Verify the token is one of a kind and it's not deleted.

    UserSession.find(
      {
        _id: token,
        isDeleted: false
      },
      (err, sessions) => {
        if (err) {
          console.log(err);
          return res.send({
            success: false,
            message: "Error: Server error"
          });
        }

        if (sessions.length != 1) {
          return res.send({
            success: false,
            message: "Error: Invalid"
          });
        } else {
          return res.send({
            success: true,
            message: "Good"
          });
        }
      }
    );
  });

  app.get("/api/account/logout", (req, res, next) => {
    // Get the token
    const { query } = req;
    const { token } = query;
    // ?token=test

    // Verify the token is one of a kind and it's not deleted.

    UserSession.findOneAndUpdate(
      {
        _id: token,
        isDeleted: false
      },
      {
        $set: {
          isDeleted: true
        }
      },
      null,
      (err, sessions) => {
        if (err) {
          console.log(err);
          return res.send({
            success: false,
            message: "Error: Server error"
          });
        }

        return res.send({
          success: true,
          message: "Good"
        });
      }
    );
  });
};
