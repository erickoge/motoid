const User = require("../models/User");
const UserSession = require("../models/UserSession");
var util = require("util");

const userID = (req, res, next, id) => {
  UserSession.find(
    {
      //id is tokken
      _id: id,
      isDeleted: false
    },
    (err, sessions) => {
      if (err || !sessions)
        return res.status("400").json({
          error: "User not found"
        });
      req.user = sessions;
      console.log("sessions==>" + util.inspect(sessions));
      next();
    }
  );
};
module.exports = { userID };
