const Product = require("../models/product");
const Province = require("../models/Province");
const Kabkot = require("../models/Kabkota");
const formidable = require("formidable");
var util = require("util");
const fs = require("fs");
const path = require("path");
const errorHandler = require("./../helpers/dbErrorHandler");

const suggestMotor = (req, res) => {
  Product.find({}, (err, products) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      });
    }
    console.log("products=>" + util.inspect(products));
    res.json(products);
  })
  .populate([{path:"kategoris"}])
    .sort({ created: -1 })
    .limit(6);
};
const list = (req, res) => {
  const queryTitle = {};
  const queryTag = {};
  if (req.query.search)
    queryTitle.title = { $regex: req.query.search, $options: "i" };
  queryTag.tag = { $regex: req.query.search, $options: "i" };
  //if (req.query.category && req.query.category != "All")
  //  query.category = req.query.category;
  console.log("query=>" + util.inspect([queryTitle, queryTag]));
  Product.find({ $or: [queryTitle, queryTag] }, (err, products) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      });
    }
    console.log("products=>" + util.inspect(products));
    res.json(products);
  })
    .populate([{ path: "city" }, { path: "kategoris" }, { path: "province" }])
    // .populate("shop", "_id name")
    .select("-image");
};
const photo = (req, res, next) => {
  if (req.product.file1.data) {
    res.set("Content-Type", req.product.file1.contentType);
    return res.send(req.product.file1.data);
  }
  next();
};
const defaultPhoto = (req, res) => {
  return res.sendFile(path.resolve("./client/public/assets/img/250x250.jpg"));
  // process.cwd() + path.resolve("client/public/assets/img/250x250.jpg")
  // );
};
const listByuserId = (req, res) => {
  //console.log("req.userId=>" + util.inspect(req.params.userId));
  Product.find({ user: req.params.userId }, (err, products) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      });
    }
    // console.log("req.userId=>" + util.inspect(products));
    res.json(products);
  })
    .populate([{ path: "city" }, { path: "kategoris" }, { path: "province" }])
    //.populate("city kategoris province")
    .sort("-created")
    .select("-image");
};

const productByID = (req, res, next, id) => {
  console.log("productByID call2!!");
  Product.findById(id)
    //.populate("shop", "_id name")
    .exec((err, product) => {
      if (err || !product)
        return res.status("400").json({
          error: "Product not found"
        });
      req.product = product;
      next();
    });
};
const create2 = (req, res, next) => {
  console.log("create call2!!");
  let form = new formidable.IncomingForm();
  let userData = req.user;
  let productData = req.product;

  form.keepExtensions = true;
  form.parse(req, (err, fields, files) => {
    if (err) {
      return res.status(400).json({
        message: "Image could not be uploaded"
      });
    }
    //console.log("req.user==>" + req.user[userId]);
    // fields.user = req.user.userId;
    // fields._id = req._id;
    let product = new Product(fields);
    console.log("req._id=> " + req.product._id);
    console.log("req._id=> " + util.inspect(files.file1));
    if (files.file1) {
      product.file1.data = fs.readFileSync(files.file1.path);
      product.file1.contentType = files.file1.type;
    }
    if (files.file2) {
      product.file2.data = fs.readFileSync(files.file2.path);
      product.file2.contentType = files.file2.type;
    }
    if (files.file3) {
      product.file3.data = fs.readFileSync(files.file3.path);
      product.file3.contentType = files.file3.type;
    }
    if (files.file4) {
      product.file4.data = fs.readFileSync(files.file4.path);
      product.file4.contentType = files.file4.type;
    }
    if (files.file5) {
      product.file5.data = fs.readFileSync(files.file5.path);
      product.file5.contentType = files.file5.type;
    }
    //product.user = userData[0].userId;
    Product.findOneAndUpdate(
      { _id: req.product._id },
      { $set: product },
      { new: true },
      function(err, result) {
        if (err) {
          return res.status(400).json({
            error: errorHandler.getErrorMessage(err)
          });
        }
        console.log(
          "Result is==>" + util.inspect(result) + "req._id => " + req._id
        );
        res.json(result);
      }
    );
    // product.kategoris = productData[0].kategoris;
    //  product.city = productData[0].city;
    //  product.province = productData[0].province;

    // product.user = req.user;
    // product.kategoris = req.kategoris;

    /* product.save((err, result) => {
      if (err) {
        return res.status(400).json({
          error: errorHandler.getErrorMessage(err)
        });
      }
      res.json(result);
    });*/
  });
};

const create = (req, res, next) => {
  console.log("create call!!");
  let form = new formidable.IncomingForm();
  let userData = req.user;
  form.keepExtensions = true;
  form.parse(req, (err, fields, files) => {
    if (err) {
      return res.status(400).json({
        message: "Image could not be uploaded"
      });
    }
    //console.log("req.user==>" + req.user[userId]);
    fields.user = req.user.userId;
    let product = new Product(fields);
    product.user = userData[0].userId;
    //console.log("userData_id==>" + util.inspect(userData[0]._id));

    // product.user = req.user;
    // product.kategoris = req.kategoris;
    // if (files.image) {
    //   product.image.data = fs.readFileSync(files.image.path);
    //  product.image.contentType = files.image.type;
    //}

    product.save((err, result) => {
      if (err) {
        return res.status(400).json({
          error: errorHandler.getErrorMessage(err)
        });
      }
      res.json(result);
    });
  });
};
const listProvince = (req, res, next) => {
  Province.find({}).exec((err, provinces) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      });
    }
    //console.log("province=>" + provinces);
    res.json(provinces);
  });
};
const listCity = (req, res) => {
  console.log("provId==>" + req.provId);

  Kabkot.find({ province: req.provId }).exec((err, kabkots) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      });
    }
    console.log("kabkot=>" + kabkots);
    res.json(kabkots);
  });
};
module.exports = {
  suggestMotor,
  list,
  photo,
  defaultPhoto,
  listByuserId,
  productByID,
  create2,
  create,
  listProvince,
  listCity
};
