switch (process.env.NODE_ENV) {
  case "prod":
  case "production":
    module.exports = require("./config/webpack.prod");
    break;

  case "dev":

  case "development":
  default:
    module.exports = require("./config/webpack.dev");
}

module: {
  loaders: [
    { test: /\.css$/, loader: "style-loader" },
    {
      test: /\.css$/,
      loader: "css-loader",
      query: { modules: true, localIdentName: "[name][local]_[hash:base64:5]" }
    }
  ];
}
